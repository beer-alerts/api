import {
  DataKeySpec,
  DecryptCommand,
  GenerateDataKeyCommand,
  KMSClient,
} from '@aws-sdk/client-kms';

import { decrypt, encrypt } from './crypto';

const { AWS_ACCOUNT_NUMBER } = process.env;
const KMS_KEY_ID = `arn:aws:kms:us-west-2:${AWS_ACCOUNT_NUMBER}:alias/data-encryption-generator-key`;
const KMS_CLIENT = new KMSClient({ region: 'us-west-2' });

export async function encryptBillingInfo(
  plaintextBillingInfo: BillingInfoData,
): Promise<StoredBillingInfo> {
  const { Plaintext: encryptionKey, CiphertextBlob } = await KMS_CLIENT.send(
    new GenerateDataKeyCommand({
      KeyId: KMS_KEY_ID,
      KeySpec: DataKeySpec.AES_256,
    }),
  );

  const encryptedBody = Object.entries(plaintextBillingInfo).reduce((acc, [key, value]) => {
    acc[key] = encrypt(<string>value, encryptionKey);
    return acc;
  }, <BillingInfoData>{});

  return {
    ...encryptedBody,
    encryptionKey: Buffer.from(CiphertextBlob).toString('base64'),
  };
}

export async function decryptBillingInfo(
  encryptedBillingInfo: BillingInfoData,
  encryptionKey: string,
): Promise<BillingInfoData> {
  const { Plaintext: decryptionKey } = await KMS_CLIENT.send(
    new DecryptCommand({
      CiphertextBlob: Buffer.from(encryptionKey, 'base64'),
      KeyId: KMS_KEY_ID,
    }),
  );

  return Object.entries(encryptedBillingInfo).reduce((acc, [key, value]) => {
    acc[key] = decrypt(value, decryptionKey);
    return acc;
  }, {} as BillingInfoData);
}
