import crypto from 'crypto';

const ALGORITHM = 'aes-256-gcm';
const AUTH_TAG_BYTE_LEN = 16;
const IV_BYTE_LEN = 12;

export function encrypt(message: string, key: Uint8Array): string {
  const iv = crypto.randomBytes(IV_BYTE_LEN);

  const cipher = crypto.createCipheriv(ALGORITHM, key, iv, {
    authTagLength: AUTH_TAG_BYTE_LEN,
  });

  const encryptedMessage = Buffer.concat([cipher.update(message), cipher.final()]);

  return Buffer.concat([iv, encryptedMessage, cipher.getAuthTag()]).toString('base64');
}

export function decrypt(cipherText: string, key: Uint8Array): string {
  const cipher = Buffer.from(cipherText, 'base64');

  const authTag = cipher.slice(-AUTH_TAG_BYTE_LEN);
  const iv = cipher.slice(0, IV_BYTE_LEN);
  const encryptedMessage = cipher.slice(IV_BYTE_LEN, -AUTH_TAG_BYTE_LEN);

  const decipher = crypto
    .createDecipheriv(ALGORITHM, key, iv, { authTagLength: AUTH_TAG_BYTE_LEN })
    .setAuthTag(authTag);

  return Buffer.concat([decipher.update(encryptedMessage), decipher.final()]).toString('utf8');
}
