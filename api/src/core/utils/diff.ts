export function compareProducts(
  base: WebsiteProduct[],
  compared: WebsiteProduct[],
): {
  added: WebsiteProduct[];
  removed: WebsiteProduct[];
  updated: WebsiteProduct[];
} {
  const added = [];
  const removed = [];
  const updated = [];
  const unchanged = [];

  compared.forEach(item => {
    const similar = base.find(({ name }) => item.name === name);
    if (!similar) {
      added.push(item);
    } else if (similar.status !== item.status) {
      updated.push(item);
    } else {
      unchanged.push(item);
    }
  });
  base.forEach(item => {
    if (!compared.find(other => item.name === other.name)) {
      removed.push(item);
    }
  });

  return { added, removed, updated };
}
