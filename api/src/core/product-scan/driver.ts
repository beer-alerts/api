import puppeteer from 'puppeteer';

export const generateV2Driver = (): Promise<puppeteer.Browser> =>
  puppeteer.launch({
    executablePath: 'google-chrome-stable',
    args: ['--no-sandbox', '--disable-setuid-sandbox', '--disable-dev-shm-usage'],
  });
