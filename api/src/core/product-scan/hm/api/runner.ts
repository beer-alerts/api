import axios, { AxiosError } from 'axios';

import { BASE_WEEBLY_URL, HM_WEEBLY_STORE_ID } from '../../../constants';

const NUMBER_OF_TRIES = 3;

const PRODUCT_CATEGORY_IDS = new Set([5, 6]);
const PRODUCT_CODE_REGEX = /- .*|\W|\s/gm;

export default async function loadProducts(): Promise<ScannedProduct[]> {
  return loadProductsViaApiRec(1);
}

async function loadProductsViaApiRec(attempt: number): Promise<ScannedProduct[]> {
  if (attempt > NUMBER_OF_TRIES) {
    throw Error('Took too long trying to load products');
  }

  const {
    data: { data: products },
  } = await axios.get(`${BASE_WEEBLY_URL}/store-locations/${HM_WEEBLY_STORE_ID}/products`, {
    params: { per_page: 200, include: 'categories' },
  });

  if (products.length > 0) {
    const rawBottlesAndCans = products.filter(({ categories }) => {
      const associatedProductCategories = categories.data.map(
        ({ site_category_id }) => site_category_id,
      );

      const validCategories = associatedProductCategories.filter(id =>
        PRODUCT_CATEGORY_IDS.has(id),
      );
      return validCategories.length > 0;
    });

    return rawBottlesAndCans.map(
      ({ name, badges, site_product_id, short_description }): ScannedProduct => {
        let status: ProductStatus;
        if (badges.low_stock) {
          status = 'limited';
        } else if (badges.out_of_stock) {
          status = 'out';
        } else {
          status = 'in';
        }

        return {
          name,
          status,
          productId: site_product_id,
          description: short_description,
          productCode: name.toLowerCase().replace(PRODUCT_CODE_REGEX, ''),
        };
      },
    );
  }

  return loadProductsViaApiRec(attempt + 1);
}

if (module === require.main) {
  loadProducts()
    .then(result => {
      console.log(JSON.stringify(result, null, 2));
      process.exit(0);
    })
    .catch(err => {
      console.log((<AxiosError>err).toJSON());
      process.exit(1);
    });
}
