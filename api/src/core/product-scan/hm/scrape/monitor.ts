import { Page } from 'puppeteer';

import { generateV2Driver } from '../../driver';

import {
  initialPageLoad,
  dumbButReliableElementLoadStrategy,
  URL,
  loadProductInfo,
  LOW_STOCK_INDICATOR,
  OUT_OF_STOCK_INDICATOR,
  smartButFragileElementLoadStrategy,
} from './runner';

export default async function performHealthChecks(): Promise<void> {
  const driver = await generateV2Driver();
  const testPage = await driver.newPage();

  let err;
  try {
    await _performHealthChecks(testPage);
  } catch (innerErr) {
    err = innerErr;
  } finally {
    await testPage.close();
    await driver.close();
  }
  if (err) {
    throw err;
  } else {
    console.log('The site appears to be healthy!');
  }
}

async function _performHealthChecks(website: Page) {
  try {
    await initialPageLoad(URL, website);
  } catch (err) {
    throw Error('Unable to load website');
  }
  let allSiteElems;
  try {
    let productElems;
    ({ products: productElems, fallback: allSiteElems } = await smartButFragileElementLoadStrategy(
      website,
    ));
    if (productElems.length <= 0) {
      console.warn(
        'The smart product load strategy no longer works, investigate what changed on the site',
      );
      ({ products: allSiteElems } = await dumbButReliableElementLoadStrategy(website));
    }
  } catch (err) {
    throw Error('Unable to load product elements');
  }

  let rawProductInfo: { name: string; status: string }[];
  try {
    rawProductInfo = await Promise.all(allSiteElems.map(elem => loadProductInfo(elem)));
  } catch (err) {
    throw Error('Unable to populate raw product info');
  }

  const rawProductsWithAStatus = rawProductInfo.filter(({ status }) => !!status);
  if (
    !rawProductsWithAStatus.some(
      ({ status }) =>
        status.toLocaleLowerCase() === LOW_STOCK_INDICATOR ||
        status.toLocaleLowerCase() === OUT_OF_STOCK_INDICATOR,
    )
  ) {
    throw Error('The status text on the site has most likely changed');
  }
}

if (module === require.main) {
  performHealthChecks()
    .then(() => {
      console.log('The site appears to be healthy!');
      process.exit(0);
    })
    .catch((err: Error) => {
      console.log(`An error occured when performing health checks: ${err.message}`);
      process.exit(1);
    });
}
