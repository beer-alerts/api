import { Browser, Page, ElementHandle } from 'puppeteer';

import { generateV2Driver } from '../../driver';

type RawProductInfo = { name: string; status: string };

const PRODUCT_CODE_REGEX = /- .*|\W|\s/gm;
const NON_BEER_PHRASES = [
  'bandana',
  'cana de cabra',
  'donation',
  'guest cider',
  'guest ginger beer',
  'hat',
  'hoodie',
  'jasper hill',
  'keg',
  'ohme shop',
  'mini prints',
  'anniversary pin',
  'postcard',
  'roving cheesemonger',
  'stemmed glass',
  'sticker set',
  't-shirt',
  't-shrit',
  'tank',
  'underberg',
  'waxed canvas koozie',
];

export const URL =
  'https://togo.holymountainbrewing.com/s/order?location=11ea750d9ac8923bbc9f54ab3a70da4d';
export const LOW_STOCK_INDICATOR = 'low stock';
export const OUT_OF_STOCK_INDICATOR = 'out of stock';

const NUMBER_OF_TRIES = 1;

export default async function loadProducts(): Promise<ScannedProduct[]> {
  const driver = await generateV2Driver();
  let products;
  let err;
  try {
    products = await loadProductsViaScrapeRec(1, driver);
  } catch (innerErr) {
    err = innerErr;
  } finally {
    await driver.close();
  }
  if (err) {
    throw err;
  }
  return products;
}

async function loadProductsViaScrapeRec(
  attempt: number,
  driver: Browser,
): Promise<ScannedProduct[]> {
  if (attempt > NUMBER_OF_TRIES) {
    throw Error('Took too long trying to load products');
  }
  const website = await driver.newPage();

  let productElems: ElementHandle[] = [];
  try {
    ({ products: productElems } = await initialPageLoad(URL, website).then(() =>
      smartButFragileElementLoadStrategy(website),
    ));
  } catch (err) {
    console.log(`Unexpected error when parsing Holy Mountain's site: ${err}`);
  }

  const productInfo = await Promise.all(
    productElems.map((productCardElem: ElementHandle) => loadProductInfo(productCardElem)),
  ).then(rawProductInfo => rawProductInfo.map(raw => generateWebsiteProduct(raw)));

  await website.close();
  if (productInfo.length > 0) {
    return productInfo
      .filter(
        ({ name }) => !NON_BEER_PHRASES.some(phrase => name.toLocaleLowerCase().includes(phrase)),
      )
      .sort((a, b) => a.name.localeCompare(b.name));
  }

  return loadProductsViaScrapeRec(attempt + 1, driver);
}

export async function initialPageLoad(url: string, website: Page): Promise<void> {
  await website.goto(url);
  await website.waitForSelector('div.card.product-group');
}

export async function smartButFragileElementLoadStrategy(
  website: Page,
): Promise<{ [key: string]: ElementHandle[] }> {
  // this line also serves as a way to guarantee all elements are loaded
  const { products: fallback } = await dumbButReliableElementLoadStrategy(website);

  const [cans, bottles] = await Promise.all([
    siteSectionSelector(website, 'Cans'),
    siteSectionSelector(website, 'Bottles'),
  ]);

  return { products: [...cans, ...bottles], fallback };
}

async function siteSectionSelector(website: Page, sectionPhrase: string) {
  const associatedProductsSection =
    'following-sibling::div[descendant::*[contains(@class,"card") and contains(@class,"product-group")]]';

  const [section] = await website.$x(
    `//*[descendant::*[contains(@class, "category-title__container")]//*[contains(text(), "${sectionPhrase}")]]/${associatedProductsSection}`,
  );

  return section?.$$('div.card.product-group') || [];
}

export async function dumbButReliableElementLoadStrategy(
  website: Page,
): Promise<{ [key: string]: ElementHandle[] }> {
  let currentElems: ElementHandle[] = [];
  let currentCount: number;
  do {
    currentCount = currentElems.length;
    currentElems = await website.$$('div.card.product-group');
  } while (currentCount != currentElems.length);

  return { products: currentElems, fallback: [] };
}

export async function loadProductInfo(productCard: ElementHandle): Promise<RawProductInfo> {
  const [[nameText], [statusText]] = await Promise.all([
    productCard.$$eval('.w-product-title', l => l.map((e: HTMLElement) => e.innerText)),
    productCard.$$eval('.stock-tag', l => l.map((e: HTMLElement) => e.innerText)),
  ]);

  return { name: nameText || '', status: statusText || '' };
}

function generateWebsiteProduct({ name, status: originalStatus }: RawProductInfo): ScannedProduct {
  let status: ProductStatus;
  const normalizedStatusText = originalStatus.toLowerCase();
  if (normalizedStatusText.includes(LOW_STOCK_INDICATOR)) {
    status = 'limited';
  } else if (normalizedStatusText.includes(OUT_OF_STOCK_INDICATOR)) {
    status = 'out';
  } else {
    status = 'in';
  }

  return {
    name,
    status,
    productId: '',
    description: '',
    productCode: name.toLowerCase().replace(PRODUCT_CODE_REGEX, ''),
  };
}
