import Knex from 'knex';

export async function seed(knex: Knex): Promise<void | void[]> {
  await knex('breweries').del();
  const [{ id: brewery_id }] = await knex('breweries').insert(
    {
      name: 'Holy Mountain Brewing',
      website: 'https://togo.holymountainbrewing.com',
      website_id: 'hm',
    },
    ['id'],
  );

  return knex('beer_families').insert([
    {
      abbreviation: 'hog',
      family_name: 'Hand of Glory',
      product_code: 'handofglory',
      brewery_id,
    },
    {
      abbreviation: 'ms',
      family_name: 'Midnight Still',
      product_code: 'midnightstill',
      brewery_id,
    },
    {
      abbreviation: 'table',
      family_name: 'Table Beer',
      product_code: 'table',
      brewery_id,
    },
    {
      abbreviation: 'cotv',
      family_name: 'Call of the Void',
      product_code: 'callofthevoid',
      brewery_id,
    },
    {
      abbreviation: 'v12',
      family_name: 'Vol. 12',
      product_code: 'vol12',
      brewery_id,
    },
  ]);
}
