const common = {
  client: 'postgresql',
  connection: {
    host: process.env.DB_HOST || 'localhost',
    port: process.env.DB_PORT || 5432,
    database: 'beer_alerts',
    user: 'beer_alerts',
    password: 'beer_alerts',
  },
  migrations: {
    tableName: 'knex_migrations',
    directory: 'migrations/beer_alerts',
    stub: 'migrations/stub.ts',
  },
  seeds: {
    directory: 'seeds/beer_alerts',
  },
};

module.exports = {
  development: common,
  production: {
    ...common,
    connection: {
      ...common.connection,
      user: 'beer_alerts_admin',
      password: process.env.DB_ADMIN_PASSWORD,
    },
  },
  test: {
    ...common,
    connection: {
      ...common.connection,
      database: 'beer_alerts_test',
      user: 'beer_alerts_test',
    },
  },
};
