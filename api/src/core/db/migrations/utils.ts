import Knex = require('knex/types');

export const autoUuid = (
  knex: Knex,
  table: Knex.CreateTableBuilder,
  columnName: string,
): Knex.ColumnBuilder => table.uuid(columnName).defaultTo(knex.raw('uuid_generate_v1mc()'));
