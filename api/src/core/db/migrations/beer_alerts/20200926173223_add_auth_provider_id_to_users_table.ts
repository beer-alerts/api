import Knex = require('knex/types');

export async function up(knex: Knex): Promise<void | void[]> {
  return knex.schema.table('users', table => {
    table.text('auth_provider_id');
  });
}

export async function down(knex: Knex): Promise<void | void[]> {
  return knex.schema.table('users', table => {
    table.dropColumn('auth_provider_id');
  });
}
