import Knex = require('knex/types');

export async function up(knex: Knex): Promise<void | void[]> {
  return knex.schema.createTable('billing_info', table => {
    table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'));
    table.integer('user_id').references('users.id').onDelete('CASCADE');
    table.text('first_name');
    table.text('last_name');
    table.text('email');
    table.text('phone');
    table.text('card_number');
    table.text('exp_month');
    table.text('exp_year');
    table.text('encryption_key');
  });
}

export async function down(knex: Knex): Promise<void | void[]> {
  return knex.schema.dropTable('billing_info');
}
