import Knex = require('knex/types');

import { autoUuid } from '../utils';

export async function up(knex: Knex): Promise<void | void[]> {
  return knex.schema.createTable('recent_scans', table => {
    autoUuid(knex, table, 'id');
    table.integer('brewery_id').references('breweries.id').onDelete('CASCADE');
    table.jsonb('scan_data');
    table.jsonb('diff');
    table.timestamp('scanned_at').defaultTo(knex.fn.now());
    table.index(['brewery_id', 'scanned_at']);
  });
}

export async function down(knex: Knex): Promise<void | void[]> {
  return knex.schema.dropTable('recent_scans');
}
