import Knex = require('knex/types');

export async function up(knex: Knex): Promise<void | void[]> {
  return knex.schema.table('billing_info', table => {
    table.text('zip_code');
  });
}

export async function down(knex: Knex): Promise<void | void[]> {
  return knex.schema.table('billing_info', table => {
    table.dropColumn('zip_code');
  });
}
