import Knex = require('knex/types');

export async function up(knex: Knex): Promise<void | void[]> {
  await Promise.all([
    knex.schema.createTable('breweries', table => {
      table.increments('id').primary();
      table.string('name');
      table.string('website');
      table.string('website_id');
      table.jsonb('last_scan').defaultTo(JSON.stringify({ products: [] }));
      table.timestamps(true, true);
    }),
    knex.schema.createTable('users', table => {
      table.increments('id').primary();
      table.string('name');
      table.string('email');
      table.timestamps(true, true);
    }),
  ]);

  await knex.schema.createTable('beer_families', table => {
    table.increments('id').primary();
    table.string('name');
    table.string('product_code');
    table.string('abbreviation');
    table.integer('brewery_id').references('breweries.id').onDelete('CASCADE');
    table.timestamps(true, true);
  });

  return Promise.all([
    knex.schema.createTable('beers', table => {
      table.string('name');
      table.string('product_code');
      table.integer('family_id').references('beer_families.id').onDelete('CASCADE');
      table.timestamps(true, true);
    }),
    knex.schema.createTable('subscriptions', table => {
      table.integer('user_id').references('users.id').onDelete('CASCADE');
      table.integer('subscription_id').references('beer_families.id').onDelete('CASCADE');
      table.index('user_id');
      table.timestamps(true, true);
    }),
  ]);
}

export async function down(knex: Knex): Promise<void | void[]> {
  await Promise.all([knex.schema.dropTable('beers'), knex.schema.dropTable('subscriptions')]);
  await knex.schema.dropTable('beer_families');

  return Promise.all([knex.schema.dropTable('breweries'), knex.schema.dropTable('users')]);
}
