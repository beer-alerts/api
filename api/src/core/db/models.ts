import Knex from 'knex';
import { Model as ObjectionModel, RelationMappings, knexSnakeCaseMappers } from 'objection';
import visibilityPlugin from 'objection-visibility';

import knexConfigs from './beer_alerts.knexfile';

export const knex = Knex({
  ...knexConfigs[process.env.NODE_ENV || 'development'],
  ...knexSnakeCaseMappers(),
});

ObjectionModel.knex(knex);

class BaseModel extends visibilityPlugin(ObjectionModel) {
  static get hidden(): string[] {
    return ['createdAt', 'updatedAt'];
  }
}

export class User extends BaseModel implements UserData {
  id: number;
  authProviderId: string;
  name: string;
  email: string;
  subscriptions: Subscription[] = [];
  billingInfo?: BillingInfo;

  static get tableName(): string {
    return 'users';
  }

  static get relationMappings(): RelationMappings {
    return {
      subscriptions: {
        relation: ObjectionModel.HasManyRelation,
        modelClass: Subscription,
        join: {
          from: 'users.id',
          to: 'subscriptions.userId',
        },
      },
      billingInfo: {
        relation: ObjectionModel.HasOneRelation,
        modelClass: BillingInfo,
        join: {
          from: 'users.id',
          to: 'billingInfo.userId',
        },
      },
    };
  }
}

export class BillingInfo extends BaseModel implements BillingInfoData {
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  cardNumber: string;
  expMonth: string;
  expYear: string;
  zipCode: string;
  encryptionKey: string;

  static get tableName(): string {
    return 'billingInfo';
  }

  static get jsonSchema(): unknown {
    return {
      type: 'object',
      properties: {
        firstName: { type: 'string' },
        lastName: { type: 'string' },
        email: { type: 'string' },
        phone: { type: 'string' },
        cardNumber: { type: 'string' },
        expMonth: { type: 'string' },
        expYear: { type: 'string' },
        zipCode: { type: 'string' },
      },
      required: [
        'firstName',
        'lastName',
        'email',
        'phone',
        'cardNumber',
        'expMonth',
        'expYear',
        'zipCode',
      ],
    };
  }

  static get visible(): string[] {
    return [
      'firstName',
      'lastName',
      'email',
      'phone',
      'cardNumber',
      'expMonth',
      'expYear',
      'zipCode',
    ];
  }
}

export class Brewery extends BaseModel {
  id: number;
  name: string;
  website: string;
  websiteId: string;
  lastScan: WebsiteScanResult;
  beerFamilies: BeerFamily[] = [];
  recentScans: RecentScan[] = [];

  static get tableName(): string {
    return 'breweries';
  }

  static get relationMappings(): RelationMappings {
    return {
      beerFamilies: {
        relation: ObjectionModel.HasManyRelation,
        modelClass: BeerFamily,
        join: {
          from: 'breweries.id',
          to: 'beerFamilies.breweryId',
        },
      },
      recentScans: {
        relation: ObjectionModel.HasManyRelation,
        modelClass: RecentScan,
        join: {
          from: 'breweries.id',
          to: 'recentScans.breweryId',
        },
      },
    };
  }

  static get visible(): string[] {
    return ['id', 'name', 'beerFamilies'];
  }
}

export class RecentScan extends BaseModel implements RecentScanData {
  id: string;
  breweryId: string;
  scanData: { products: WebsiteProduct[] };
  diff: {
    added: WebsiteProduct[];
    updated: WebsiteProduct[];
    removed: WebsiteProduct[];
  };
  scannedAt: Date;

  static get tableName(): string {
    return 'recentScans';
  }
}

export class BeerFamily extends BaseModel implements BeerFamilyData {
  id: string;
  breweryId: string;
  productCode: string;
  name: string;
  abbreviation: string;
  beers: Beer[] = [];

  static get tableName(): string {
    return 'beerFamilies';
  }

  static get relationMappings(): RelationMappings {
    return {
      beers: {
        relation: ObjectionModel.HasManyRelation,
        modelClass: Beer,
        join: {
          from: 'beerFamilies.id',
          to: 'beers.familyId',
        },
      },
    };
  }
}

export class Beer extends BaseModel implements BeerData {
  name: string;
  productCode: string;
  familyId: string;

  static get tableName(): string {
    return 'beers';
  }

  static get idColumn(): string {
    return null;
  }
}

export class Subscription extends BaseModel implements SubscriptionData {
  userId: number;
  subscriptionId: number;
  topic: BeerFamily;

  static get tableName(): string {
    return 'subscriptions';
  }

  static get idColumn(): string {
    return null;
  }

  static get relationMappings(): RelationMappings {
    return {
      topic: {
        relation: ObjectionModel.BelongsToOneRelation,
        modelClass: BeerFamily,
        join: {
          from: 'subscriptions.subscriptionId',
          to: 'beerFamilies.id',
        },
      },
    };
  }
}
