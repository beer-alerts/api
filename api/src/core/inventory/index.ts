import { Brewery } from '../db/models';
import { compareProducts } from '../utils/diff';

export async function getBeerInventoryChanges(
  websiteId: string,
  productLoadFunction: ProductLoadFunction,
): Promise<InventoryUpdate> {
  const [
    {
      id: breweryId,
      lastScan: { products: beersFromLastScan },
      beerFamilies: existingBeerFamilies,
    },
    beersOnSite,
  ] = await Promise.all([
    Brewery.query().where({ websiteId }).withGraphFetched('beerFamilies').first(),
    productLoadFunction(),
  ]);

  const { added, updated, removed } = compareProducts(beersFromLastScan, beersOnSite);

  const { newBeerFamilies, returningBeers } = added.reduce(
    (acc, beer) => {
      const probableBeerFamily = getPartialProductMatch(beer, existingBeerFamilies);

      if (probableBeerFamily.productCode) {
        acc.returningBeers.push({
          ...beer,
          probableFamilyCode: probableBeerFamily.productCode,
        });
      } else {
        acc.newBeerFamilies.push(beer);
      }

      return acc;
    },
    { newBeerFamilies: [], returningBeers: [] },
  );

  return {
    breweryId,
    latestProducts: beersOnSite,
    newBeerFamilies,
    returningBeers,
    updatedBeers: updated.map(beer => ({
      ...beer,
      probableFamilyCode:
        getPartialProductMatch(beer, existingBeerFamilies).productCode || beer.productCode,
    })),
    removedBeers: removed
      .filter(({ status }) => status !== 'out')
      .map(beer => ({
        ...beer,
        probableFamilyCode:
          getPartialProductMatch(beer, existingBeerFamilies).productCode || beer.productCode,
      })),
  };
}

function getPartialProductMatch(
  { productCode: productCodeToCheck }: Product,
  productCollection: Product[],
): Product {
  // The product code to check will always be at least an exact match of a given product code in a collection, but potentially might be a superset
  return (
    productCollection.find(({ productCode }) => productCodeToCheck.includes(productCode)) || {
      productCode: undefined,
    }
  );
}

export function hasAnyInventoryUpdates(inventoryUpdate: InventoryUpdate): boolean {
  const { latestProducts, newBeerFamilies, returningBeers, updatedBeers, removedBeers } =
    inventoryUpdate;
  return (
    // Holy Mountain loves to wipe out the products on their site moments before releasing a whale.
    latestProducts.length === 0,
    newBeerFamilies.length > 0 ||
      returningBeers.length > 0 ||
      updatedBeers.length > 0 ||
      removedBeers.length > 0
  );
}

export function groupInventoryUpdateBySubscriptionCode({
  latestProducts,
  newBeerFamilies,
  returningBeers,
  updatedBeers,
  removedBeers,
}: InventoryUpdate): GroupedInventoryUpdate {
  return {
    latestInventory: latestProducts,
    newBeerFamilies,
    returningBeers: groupBySubscriptionCode(returningBeers),
    updatedBeers: groupBySubscriptionCode(updatedBeers),
    removedBeers: groupBySubscriptionCode(removedBeers),
  };
}

function groupBySubscriptionCode(category: WebsiteProduct[]): {
  [subscriptionCode: string]: WebsiteProduct[];
} {
  return category.reduce((acc, product) => {
    console.log(product.probableFamilyCode);
    const group = acc[product.probableFamilyCode] || [];

    group.push(product);
    acc[product.probableFamilyCode] = group;

    return acc;
  }, {});
}
