import { Brewery } from '../db/models';

import { getBeerInventoryChanges } from '.';

describe('Beer Inventory Retrieval Tests', () => {
  it('detects when a never-before-seen beer family is present in a scan', async () => {
    await Brewery.query().insert({
      websiteId: 'test',
      lastScan: { products: [] },
    });
    const stubbedProductLoad: ProductLoadFunction = async () => [
      {
        name: 'Test Beer',
        productCode: 'testbeer',
        productId: '1',
        description: 'testing',
        status: 'limited',
      },
    ];

    const inventoryChanges = await getBeerInventoryChanges('test', stubbedProductLoad);

    expect(inventoryChanges).toMatchObject({
      latestProducts: [
        {
          name: 'Test Beer',
          productCode: 'testbeer',
          status: 'limited',
        },
      ],
      newBeerFamilies: [
        {
          name: 'Test Beer',
          productCode: 'testbeer',
          status: 'limited',
        },
      ],
      returningBeers: [],
    });
  });

  it('detects when an existing beer family is present in a scan', async () => {
    await Brewery.query().insertGraph({
      websiteId: 'test',
      lastScan: { products: [] },
      beerFamilies: [{ productCode: 'testbeer' }],
    });

    const stubbedProductLoad: ProductLoadFunction = async () => [
      {
        name: 'Test Beer',
        productCode: 'testbeer',
        productId: '1',
        description: 'testing',
        status: 'in',
      },
    ];

    const inventoryChanges = await getBeerInventoryChanges('test', stubbedProductLoad);

    expect(inventoryChanges).toMatchObject({
      latestProducts: [{ name: 'Test Beer', productCode: 'testbeer', status: 'in' }],
      newBeerFamilies: [],
      returningBeers: [{ name: 'Test Beer', productCode: 'testbeer', status: 'in' }],
    });
  });

  it('detects when a beer family has its status updated to "limited"', async () => {
    await Brewery.query().insertGraph({
      websiteId: 'test',
      lastScan: {
        products: [
          {
            name: 'Test Beer',
            productCode: 'testbeer',
            productId: '1',
            status: 'in',
            description: 'a test',
          },
        ],
      },
      beerFamilies: [{ name: 'Test Family', productCode: 'testbeer' }],
    });

    const stubbedProductLoad: ProductLoadFunction = async () => [
      {
        name: 'Test Beer',
        productCode: 'testbeer',
        productId: '1',
        description: 'testing',
        status: 'limited',
      },
    ];

    const inventoryChanges = await getBeerInventoryChanges('test', stubbedProductLoad);

    expect(inventoryChanges).toMatchObject({
      latestProducts: [{ name: 'Test Beer', productCode: 'testbeer', status: 'limited' }],
      updatedBeers: [{ name: 'Test Beer', productCode: 'testbeer', status: 'limited' }],
    });
  });

  it('detects when a beer family has its status updated to "out"', async () => {
    await Brewery.query().insertGraph({
      websiteId: 'test',
      lastScan: {
        products: [
          {
            name: 'Test Beer',
            productCode: 'testbeer',
            description: 'a test',
            productId: '1',
            status: 'in',
          },
        ],
      },
      beerFamilies: [{ name: 'Test Family', productCode: 'testbeer' }],
    });

    const stubbedProductLoad: ProductLoadFunction = async () => [
      {
        name: 'Test Beer',
        productCode: 'testbeer',
        productId: '1',
        description: 'testing',
        status: 'out',
      },
    ];

    const inventoryChanges = await getBeerInventoryChanges('test', stubbedProductLoad);

    expect(inventoryChanges).toMatchObject({
      latestProducts: [{ name: 'Test Beer', productCode: 'testbeer', status: 'out' }],
      updatedBeers: [{ name: 'Test Beer', productCode: 'testbeer', status: 'out' }],
    });
  });

  it('detects when a beer family has been removed from the website', async () => {
    await Brewery.query().insertGraph({
      websiteId: 'test',
      lastScan: {
        products: [
          {
            name: 'Test Beer',
            productCode: 'testbeer',
            description: 'a test',
            productId: '1',
            status: 'in',
          },
        ],
      },
      beerFamilies: [{ name: 'Test Family', productCode: 'testbeer' }],
    });

    const stubbedProductLoad: ProductLoadFunction = async () => [];

    const inventoryChanges = await getBeerInventoryChanges('test', stubbedProductLoad);

    expect(inventoryChanges).toMatchObject({
      latestProducts: [],
      removedBeers: [{ name: 'Test Beer', productCode: 'testbeer', status: 'in' }],
    });
  });

  it('makes a best guess as to the product code of the beer family a new beer belongs to', async () => {
    await Brewery.query().insertGraph({
      websiteId: 'test',
      lastScan: { products: [] },
      beerFamilies: [{ productCode: 'testbeer' }],
    });

    const stubbedProductLoad: ProductLoadFunction = async () => [
      {
        name: 'Test Beer',
        productCode: 'testbeervariant',
        productId: '1',
        description: 'testing',
        status: 'in',
      },
    ];

    const inventoryChanges = await getBeerInventoryChanges('test', stubbedProductLoad);

    expect(inventoryChanges).toMatchObject({
      latestProducts: [
        {
          name: 'Test Beer',
          productCode: 'testbeervariant',
          status: 'in',
        },
      ],
      newBeerFamilies: [],
      returningBeers: [
        {
          name: 'Test Beer',
          productCode: 'testbeervariant',
          probableFamilyCode: 'testbeer',
          status: 'in',
        },
      ],
    });
  });

  it('makes a best guess as to the product code of the beer family an updated beer belongs to', async () => {
    await Brewery.query().insertGraph({
      websiteId: 'test',
      lastScan: {
        products: [
          {
            name: 'Test Beer',
            productCode: 'testbeervariant',
            description: 'a test',
            productId: '1',
            status: 'in',
          },
        ],
      },
      beerFamilies: [{ productCode: 'testbeer' }],
    });

    const stubbedProductLoad: ProductLoadFunction = async () => [
      {
        name: 'Test Beer',
        productCode: 'testbeervariant',
        productId: '1',
        description: 'testing',
        status: 'limited',
      },
    ];

    const inventoryChanges = await getBeerInventoryChanges('test', stubbedProductLoad);

    expect(inventoryChanges).toMatchObject({
      latestProducts: [
        {
          name: 'Test Beer',
          productCode: 'testbeervariant',
          status: 'limited',
        },
      ],
      updatedBeers: [
        {
          name: 'Test Beer',
          productCode: 'testbeervariant',
          probableFamilyCode: 'testbeer',
          status: 'limited',
        },
      ],
    });
  });

  it('makes a best guess as to the product code of the beer family a removed beer belongs to', async () => {
    await Brewery.query().insertGraph({
      websiteId: 'test',
      lastScan: {
        products: [
          {
            name: 'Test Beer',
            productCode: 'testbeervariant',
            description: 'a test',
            productId: '1',
            status: 'in',
          },
        ],
      },
      beerFamilies: [{ productCode: 'testbeer' }],
    });

    const stubbedProductLoad: ProductLoadFunction = async () => [];

    const inventoryChanges = await getBeerInventoryChanges('test', stubbedProductLoad);

    expect(inventoryChanges).toMatchObject({
      latestProducts: [],
      removedBeers: [
        {
          name: 'Test Beer',
          productCode: 'testbeervariant',
          probableFamilyCode: 'testbeer',
          status: 'in',
        },
      ],
    });
  });

  it('omits results from "removed" that had a previous status of "out"', async () => {
    await Brewery.query().insertGraph({
      websiteId: 'test',
      lastScan: {
        products: [
          {
            name: 'Test Beer',
            productCode: 'testbeervariant',
            description: 'a test',
            productId: '1',
            status: 'out',
          },
        ],
      },
      beerFamilies: [{ productCode: 'testbeer' }],
    });

    const stubbedProductLoad: ProductLoadFunction = async () => [];

    const inventoryChanges = await getBeerInventoryChanges('test', stubbedProductLoad);

    expect(inventoryChanges).toMatchObject({
      latestProducts: [],
      removedBeers: [],
    });
  });
});
