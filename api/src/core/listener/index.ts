import { Brewery, User } from '../db/models';
import {
  getBeerInventoryChanges,
  groupInventoryUpdateBySubscriptionCode,
  hasAnyInventoryUpdates,
} from '../inventory';
import {
  aggregateRelevantUserSubscriptions,
  createSubscriptionCodeLookup,
  hasAnySubscriptionUpdates,
} from '../subscriptions';
import { sendUpdateEmail } from '../email/index';
import { commitAllBreweryInventoryChanges } from '../brewery-updates';
import { updateBreweryRecentScanWindow } from '../recent-scans';

export async function run(): Promise<void> {
  const [allUsers, allBreweries] = await Promise.all([
    User.query().withGraphFetched('[subscriptions.topic, billingInfo]'),
    Brewery.query(),
  ]);

  const inventoryUpdates: InventoryUpdate[] = await Promise.all(
    allBreweries.map(async brewery => {
      console.log(`Loading products from ${brewery.name}...`);
      const { loadProducts }: { loadProducts: ProductLoadFunction } = await import(
        `../product-scan/${brewery.websiteId}`
      );
      const inventoryChanges = await getBeerInventoryChanges(brewery.websiteId, loadProducts);
      await updateBreweryRecentScanWindow(brewery, {
        scanData: { products: inventoryChanges.latestProducts },
        diff: {
          added: [...inventoryChanges.newBeerFamilies, ...inventoryChanges.returningBeers],
          updated: inventoryChanges.updatedBeers,
          removed: inventoryChanges.removedBeers,
        },
      });

      return inventoryChanges;
    }),
  );

  const updatesByBrewery: InventoryByBrewery = inventoryUpdates
    .filter(inventoryUpdates => hasAnyInventoryUpdates(inventoryUpdates))
    .reduce(
      (acc, inventoryUpdate, i) =>
        Object.assign(acc, {
          [inventoryUpdate.breweryId]: {
            name: allBreweries[i].name,
            website: allBreweries[i].website,
            latestProducts: inventoryUpdate.latestProducts,
            inventoryUpdates: groupInventoryUpdateBySubscriptionCode(inventoryUpdate),
          },
        }),
      {} as InventoryByBrewery,
    );

  if (Object.keys(updatesByBrewery).length > 0) {
    console.log(
      [
        'The following breweries have updates:',
        ...Object.values(updatesByBrewery).map(
          ({ name, inventoryUpdates }) => `- ${name} (${createBreweryStatus(inventoryUpdates)})`,
        ),
      ].join('\n'),
    );
  } else {
    console.log('No breweries have updates, nothing left to do');
    return;
  }

  const subscriptionsByUser = allUsers
    .map(({ subscriptions, ...userInfo }) => {
      const subscriptionLookup = createSubscriptionCodeLookup(subscriptions);
      return {
        userInfo,
        subscriptions: aggregateRelevantUserSubscriptions(subscriptionLookup, updatesByBrewery),
      };
    })
    .filter(({ subscriptions }) => hasAnySubscriptionUpdates(subscriptions));

  if (Object.keys(subscriptionsByUser).length > 0) {
    console.log(
      [
        'The following users have updates to their subscriptions:',
        ...Object.keys(subscriptionsByUser).map(
          key => `- ${subscriptionsByUser[key].userInfo.name}`,
        ),
      ].join('\n'),
    );
  } else {
    console.log('No users have relevant subscriptions that have been updated');
  }

  await commitAllBreweryInventoryChanges(updatesByBrewery, () =>
    Promise.all(subscriptionsByUser.map(user => sendUpdateEmail(user))),
  );
}

function createBreweryStatus(updates: GroupedInventoryUpdate): string {
  if (updates.latestInventory.length === 0) {
    return 'Empty inventory';
  }
  return `added: ${
    updates.newBeerFamilies.length + Object.keys(updates.returningBeers).length
  }, updated: ${Object.keys(updates.updatedBeers).length}, removed: ${
    Object.keys(updates.removedBeers).length
  }`;
}

if (module === require.main) {
  console.log(`Listening for new beers at ${new Date().toISOString()}`);
  run()
    .then(() => process.exit(0))
    .catch(err => {
      console.log(err);
      process.exit(1);
    });
}
