import * as productScan from '../product-scan/hm/api';
import * as email from '../email';
import { BeerFamily, Brewery, User } from '../db/models';

import { run } from '.';

type DeepPartial<T> = {
  [P in keyof T]?: DeepPartial<T[P]>;
};

jest.mock('../product-scan/hm/api');
const mockedProductScan = <jest.Mocked<typeof productScan>>productScan;

jest.mock('../email');
const mockedEmail = <jest.Mocked<typeof email>>email;

describe('Listener Integration Tests', () => {
  let testBrewery: Brewery;
  let testUser: User;

  beforeEach(async () => {
    [testBrewery, testUser] = await Promise.all([
      Brewery.query().insert({
        name: 'Holy Mountain',
        websiteId: 'hm',
      }),
      User.query().insert({ name: 'Beer Bro', email: 'foo@bar.com' }),
    ]);
    const beerFamily = await testBrewery
      .$relatedQuery('beerFamilies')
      .insert({ name: 'Midnight Still', productCode: 'midnightstill' });
    await testUser
      .$relatedQuery('subscriptions')
      .insert({ subscriptionId: parseInt(beerFamily.id) });
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('correctly triggers a beer family root replacement', async () => {
    mockedProductScan.loadProducts.mockResolvedValue([
      {
        name: 'Deadfall',
        productCode: 'deadfall',
        description: 'it is deadfall',
        productId: '1',
        status: 'in',
      },
    ]);
    await BeerFamily.query().insert({
      breweryId: testBrewery.id.toString(),
      name: '2018 Deadfall',
      productCode: '2018deadfall',
    });

    await run();

    const updatedFamily = await BeerFamily.query()
      .where('productCode', 'deadfall')
      .withGraphFetched('beers')
      .first();
    expect(updatedFamily.toJSON()).toMatchObject({
      name: 'Deadfall',
      productCode: 'deadfall',
      beers: [{ name: '2018 Deadfall', productCode: '2018deadfall' }],
    });
    expect(mockedEmail.sendUpdateEmail.mock.calls[0][0]).toMatchObject<
      DeepPartial<{ userInfo: UserData; subscriptions: SubscriptionUpdatesForUser }>
    >({
      userInfo: { name: 'Beer Bro', email: 'foo@bar.com' },
      subscriptions: { 'Holy Mountain': { updates: { newBeerFamilies: [{ name: 'Deadfall' }] } } },
    });
  });

  it('adds a beer to the database if it is also the family root', async () => {
    mockedProductScan.loadProducts.mockResolvedValue([
      {
        name: 'Deadfall',
        productCode: 'deadfall',
        description: 'it is deadfall',
        productId: '1',
        status: 'in',
      },
    ]);
    const beerFamily = await BeerFamily.query().insert({
      breweryId: testBrewery.id.toString(),
      name: 'Deadfall',
      productCode: 'deadfall',
    });
    await beerFamily
      .$relatedQuery('beers')
      .insert([{ name: '2018 Deadfall', productCode: '2018deadfall' }]);

    await run();

    const updatedFamily = await BeerFamily.query()
      .where('productCode', 'deadfall')
      .withGraphFetched('beers')
      .first();
    expect(updatedFamily.toJSON()).toMatchObject({
      name: 'Deadfall',
      productCode: 'deadfall',
      beers: [
        { name: '2018 Deadfall', productCode: '2018deadfall' },
        { name: 'Deadfall', productCode: 'deadfall' },
      ],
    });
  });

  it('sends out a blast email when it finds zero products in inventory', async () => {
    await testBrewery.$query().update({
      lastScan: {
        products: [
          {
            name: 'Basic Beer',
            description: 'A basic beer',
            productCode: 'basicbeer',
            productId: '123',
            status: 'in',
          },
        ],
      },
    });
    mockedProductScan.loadProducts.mockResolvedValue([]);

    await run();

    expect(mockedEmail.sendUpdateEmail).toHaveBeenCalled();
    expect(mockedEmail.sendUpdateEmail.mock.calls[0][0]).toMatchObject<
      DeepPartial<{ userInfo: UserData; subscriptions: SubscriptionUpdatesForUser }>
    >({
      userInfo: { name: 'Beer Bro', email: 'foo@bar.com' },
      subscriptions: { 'Holy Mountain': { updates: { emptyInventory: true } } },
    });
  });

  it('will not send out a "no inventory" blast email more than once consecutively', async () => {
    await testBrewery.$query().update({ lastScan: { products: [] } });
    mockedProductScan.loadProducts.mockResolvedValue([]);

    await run();

    expect(mockedEmail.sendUpdateEmail).not.toHaveBeenCalled();
  });

  it('will send relevant emails out when a beer goes from available to removed from the site', async () => {
    await testBrewery.$query().update({
      lastScan: {
        products: [
          {
            name: 'Midnight Still',
            productCode: 'midnightstill',
            description: 'the holy grail',
            productId: '123',
            status: 'in',
          },
        ],
      },
    });
    mockedProductScan.loadProducts.mockResolvedValue([
      {
        name: 'Primordial Sky',
        description: 'A sky that is primordial',
        productCode: 'primordialsky',
        productId: '456',
        status: 'in',
      },
    ]);

    await run();

    expect(mockedEmail.sendUpdateEmail.mock.calls[0][0]).toMatchObject<
      DeepPartial<{ userInfo: UserData; subscriptions: SubscriptionUpdatesForUser }>
    >({
      userInfo: { name: 'Beer Bro', email: 'foo@bar.com' },
      subscriptions: {
        'Holy Mountain': {
          updates: { emptyInventory: false, removedBeers: [{ name: 'Midnight Still' }] },
        },
      },
    });
  });

  test('A user with billing info will have it passed to the email construction service', async () => {
    await testUser.$relatedQuery('billingInfo').insert({
      firstName: 'Beer',
      lastName: 'Bro',
      email: '',
      phone: '',
      cardNumber: '',
      expMonth: '',
      expYear: '',
      zipCode: '',
    });

    mockedProductScan.loadProducts.mockResolvedValue([
      {
        name: 'Deadfall',
        productCode: 'deadfall',
        description: 'it is deadfall',
        productId: '1',
        status: 'in',
      },
    ]);

    await run();

    expect(mockedEmail.sendUpdateEmail.mock.calls[0][0]).toMatchObject<
      DeepPartial<{ userInfo: UserData; subscriptions: SubscriptionUpdatesForUser }>
    >({
      userInfo: {
        name: 'Beer Bro',
        billingInfo: { firstName: 'Beer', lastName: 'Bro' },
      },
    });
  });
});
