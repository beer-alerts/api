import { Transaction } from 'knex';

import { knex, BeerFamily, Brewery } from '../db/models';

export async function commitAllBreweryInventoryChanges(
  allInventoryChanges: InventoryByBrewery,
  finalizationFn: () => Promise<void | void[]>,
): Promise<void> {
  await knex.transaction(async trx => {
    await Promise.all(
      Object.keys(allInventoryChanges).map(breweryId =>
        commitBreweryInventoryChanges(
          breweryId,
          allInventoryChanges[breweryId].latestProducts,
          allInventoryChanges[breweryId].inventoryUpdates,
          trx,
        ),
      ),
    );
    await finalizationFn();
  });
}

async function commitBreweryInventoryChanges(
  breweryId: string,
  latestProducts: ScannedProduct[],
  { newBeerFamilies, returningBeers }: GroupedInventoryUpdate,
  trx: Transaction,
): Promise<unknown> {
  await Promise.all([
    commitLatestBreweryScan(breweryId, latestProducts, trx),
    ...Object.keys(returningBeers).map(async familyCode =>
      commitAdditionsToBeerFamily(breweryId, familyCode, returningBeers[familyCode], trx),
    ),
  ]);

  return Promise.all(
    newBeerFamilies.map(async ({ name, productCode }) => {
      const familyCandidates = await BeerFamily.query()
        .where('productCode', 'like', `%${productCode}%`)
        .orderBy('created_at')
        .transacting(trx);
      if (familyCandidates.length > 1) {
        console.log(
          'Unexpected state: more than one possible family root candidate - selecting the older one',
        );
      }
      const [probableFamilyRoot] = familyCandidates;
      if (probableFamilyRoot) {
        const { name: oldFamilyName, productCode: oldFamilyProductCode } = probableFamilyRoot;
        const updatedFamilyRoot: BeerFamily = await probableFamilyRoot
          .$query()
          .patchAndFetch({ name, productCode })
          .transacting(trx);
        return updatedFamilyRoot
          .$relatedQuery('beers')
          .insert({ name: oldFamilyName, productCode: oldFamilyProductCode })
          .transacting(trx);
      }
      return BeerFamily.query().insert({ breweryId, name, productCode }).transacting(trx);
    }),
  );
}

async function commitAdditionsToBeerFamily(
  breweryId: string,
  familyCode: string,
  newBeersInFamily: WebsiteProduct[],
  trx: Transaction,
) {
  const beerFamily = await BeerFamily.query()
    .where({ breweryId, productCode: familyCode })
    .withGraphFetched('beers')
    .first()
    .transacting(trx);
  const beers = newBeersInFamily
    .filter(beer => !beerFamily.beers.some(({ productCode }) => beer.productCode === productCode))
    .map(({ name, productCode }) => ({ name, productCode }));

  return beerFamily.$relatedQuery('beers').insert(beers).transacting(trx);
}

async function commitLatestBreweryScan(
  id: string,
  latestProducts: ScannedProduct[],
  trx: Transaction,
): Promise<unknown> {
  return Brewery.query()
    .where({ id })
    .update({ lastScan: { products: latestProducts } })
    .transacting(trx);
}
