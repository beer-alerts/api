import { Brewery, BeerFamily } from '../db/models';

import { commitAllBreweryInventoryChanges } from '.';

describe('Beer Inventory Commit Logic Tests', () => {
  const finalizationFn = jest.fn();

  beforeEach(() => {
    finalizationFn.mockReset();
  });

  it('adds new beer families to breweries when encountered', async () => {
    const brewery = await Brewery.query().insert({});

    await commitAllBreweryInventoryChanges(
      {
        [brewery.id]: {
          name: '',
          website: '',
          latestProducts: [],
          inventoryUpdates: {
            latestInventory: [],
            newBeerFamilies: [
              {
                productCode: 'productcode',
                productId: '1',
                name: 'Beer Name',
                status: 'limited',
              },
            ],
            returningBeers: {},
            updatedBeers: {},
            removedBeers: {},
          },
        },
      },
      finalizationFn,
    );

    expect(await brewery.$relatedQuery('beerFamilies')).toMatchObject([
      {
        breweryId: brewery.id,
        name: 'Beer Name',
        productCode: 'productcode',
      },
    ]);
  });

  it('adds new beers to existing beer families when encountered', async () => {
    const brewery = await Brewery.query().insertGraphAndFetch({
      beerFamilies: [{ name: 'The existing family', productCode: 'familyproductcode' }],
    });

    await commitAllBreweryInventoryChanges(
      {
        [brewery.id]: {
          name: '',
          website: '',
          latestProducts: [],
          inventoryUpdates: {
            latestInventory: [],
            newBeerFamilies: [],
            returningBeers: {
              familyproductcode: [
                {
                  productCode: 'productcode',
                  productId: '1',
                  name: 'Beer Name',
                  status: 'limited',
                },
              ],
            },
            updatedBeers: {},
            removedBeers: {},
          },
        },
      },
      finalizationFn,
    );

    expect(
      await BeerFamily.query().where({ breweryId: brewery.id }).withGraphFetched('beers').first(),
    ).toMatchObject({
      breweryId: brewery.id,
      name: 'The existing family',
      productCode: 'familyproductcode',
      beers: [
        { familyId: brewery.beerFamilies[0].id, productCode: 'productcode', name: 'Beer Name' },
      ],
    });
  });

  it('does not add duplicate entries for existing beers in a beer family', async () => {
    const brewery = await Brewery.query().insertGraph({
      beerFamilies: [
        {
          name: 'The existing family',
          productCode: 'familyproductcode',
          beers: [{ name: 'Existing Beer', productCode: 'productcode' }],
        },
      ],
    });

    await commitAllBreweryInventoryChanges(
      {
        [brewery.id]: {
          name: '',
          website: '',
          latestProducts: [],
          inventoryUpdates: {
            latestInventory: [],
            newBeerFamilies: [],
            returningBeers: {
              familyproductcode: [
                {
                  productCode: 'productcode',
                  productId: '1',
                  name: 'Existing Beer',
                  status: 'limited',
                },
                {
                  productCode: 'thenewvariant',
                  productId: '2',
                  name: 'The New Variant',
                  status: 'limited',
                },
              ],
            },
            updatedBeers: {},
            removedBeers: {},
          },
        },
      },
      finalizationFn,
    );

    expect(
      await BeerFamily.query()
        .where({ breweryId: brewery.id })
        .withGraphFetched('beers')
        .modifyGraph('beers', builder => {
          builder.orderBy('name');
        })
        .first(),
    ).toMatchObject({
      breweryId: brewery.id,
      name: 'The existing family',
      productCode: 'familyproductcode',
      beers: [
        { productCode: 'productcode', name: 'Existing Beer' },
        { productCode: 'thenewvariant', name: 'The New Variant' },
      ],
    });
  });

  it('calls the finalization function in a transaction', async () => {
    const brewery = await Brewery.query().insert({});

    await commitAllBreweryInventoryChanges(
      {
        [brewery.id]: {
          name: '',
          website: '',
          latestProducts: [],
          inventoryUpdates: {
            latestInventory: [],
            newBeerFamilies: [
              {
                productCode: 'productcode',
                productId: '1',
                name: 'Beer Name',
                status: 'limited',
              },
            ],
            returningBeers: {},
            updatedBeers: {},
            removedBeers: {},
          },
        },
      },
      finalizationFn,
    );

    expect(finalizationFn).toHaveBeenCalledTimes(1);
  });

  it('rolls back changes if the finalization function fails', async () => {
    finalizationFn.mockImplementation(() => Promise.reject('An error occurred during cleanup'));

    const brewery = await Brewery.query().insert({});

    try {
      await commitAllBreweryInventoryChanges(
        {
          [brewery.id]: {
            name: '',
            website: '',
            latestProducts: [],
            inventoryUpdates: {
              latestInventory: [],
              newBeerFamilies: [
                {
                  productCode: 'productcode',
                  productId: '1',
                  name: 'Beer Name',
                  status: 'limited',
                },
              ],
              returningBeers: {},
              updatedBeers: {},
              removedBeers: {},
            },
          },
        },
        finalizationFn,
      );

      fail('Expecting inventory changes to fail during finalization');
    } catch (err) {
      expect(finalizationFn).toHaveBeenCalledTimes(1);
      expect(await BeerFamily.query().where({ breweryId: brewery.id })).toHaveLength(0);
    }
  });

  it('Commits the latest scan of the website', async () => {
    const brewery = await Brewery.query().insert({});

    await commitAllBreweryInventoryChanges(
      {
        [brewery.id]: {
          name: '',
          website: '',
          latestProducts: [
            {
              name: 'New Stuff',
              status: 'in',
              productCode: 'newstuff',
              productId: '1',
              description: 'this is new stuff',
            },
          ],
          inventoryUpdates: {
            latestInventory: [],
            newBeerFamilies: [
              {
                productCode: 'productcode',
                productId: '2',
                name: 'Beer Name',
                status: 'limited',
              },
            ],
            returningBeers: {},
            updatedBeers: {},
            removedBeers: {},
          },
        },
      },
      finalizationFn,
    );

    expect(await brewery.$query()).toMatchObject({
      lastScan: {
        products: [{ name: 'New Stuff', status: 'in', productCode: 'newstuff' }],
      },
    });
  });

  it('can rebalance existing beer families by choosing the smallest common "productCode" from incoming beers', async () => {
    const brewery = await Brewery.query().insertGraph({
      beerFamilies: [
        {
          name: 'It Is The Beer Variant',
          productCode: 'itisthebeervariant',
          beers: [
            {
              name: 'It Is The Beer Variant with Other Stuff',
              productCode: 'itisthebeervariantwithotherstuff',
            },
          ],
        },
      ],
    });

    await commitAllBreweryInventoryChanges(
      {
        [brewery.id]: {
          name: '',
          website: '',
          latestProducts: [],
          inventoryUpdates: {
            latestInventory: [],
            // Any beer coming in whose product code is a subset of existing beer families (midnightstill does not include the code midnightstillhazelnutcacao) will be dumped in newBeerFamilies
            newBeerFamilies: [
              {
                productCode: 'thebeer',
                productId: '1',
                name: 'The Beer',
                status: 'limited',
              },
            ],
            returningBeers: {},
            updatedBeers: {},
            removedBeers: {},
          },
        },
      },
      finalizationFn,
    );

    const { beerFamilies } = await brewery
      .$query()
      .withGraphFetched('beerFamilies.beers')
      .modifyGraph('beers', beers => beers.orderBy('name'));
    expect(beerFamilies).toMatchObject([
      {
        name: 'The Beer',
        productCode: 'thebeer',
        beers: [
          {
            familyId: beerFamilies[0].id,
            name: 'It Is The Beer Variant with Other Stuff',
            productCode: 'itisthebeervariantwithotherstuff',
          },
          {
            familyId: beerFamilies[0].id,
            name: 'It Is The Beer Variant',
            productCode: 'itisthebeervariant',
          },
        ],
      },
    ]);
  });
});
