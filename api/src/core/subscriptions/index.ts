import { Subscription } from '../db/models';

export function createSubscriptionCodeLookup(
  userSubscriptions: Subscription[],
): SubscriptionCodeLookup {
  return userSubscriptions.reduce((acc, { topic: { productCode, breweryId } }) => {
    const subscriptionsForBrewery: Set<string> = acc[breweryId] || new Set();

    subscriptionsForBrewery.add(productCode);
    acc[breweryId] = subscriptionsForBrewery;

    return acc;
  }, {});
}

export function aggregateRelevantUserSubscriptions(
  userSubscriptions: SubscriptionCodeLookup,
  breweryInventoryUpdates: InventoryByBrewery,
): SubscriptionUpdatesForUser {
  return Object.keys(userSubscriptions).reduce((acc, breweryId) => {
    const {
      name,
      website,
      inventoryUpdates: {
        latestInventory,
        newBeerFamilies,
        returningBeers,
        updatedBeers,
        removedBeers,
      },
    } = breweryInventoryUpdates[breweryId];

    acc[name] = {
      websiteUrl: website,
      updates: {
        emptyInventory: latestInventory.length === 0,
        newBeerFamilies,
        returningBeers: matchSubscriptions(returningBeers, userSubscriptions[breweryId]),
        updatedBeers: matchSubscriptions(updatedBeers, userSubscriptions[breweryId]),
        removedBeers: matchSubscriptions(removedBeers, userSubscriptions[breweryId]),
      },
    };

    return acc;
  }, {} as SubscriptionUpdatesForUser);
}

function matchSubscriptions(
  inventoryGroups: ProductGroups,
  userSubscriptions: Set<string>,
): WebsiteProduct[] {
  return Object.keys(inventoryGroups)
    .filter(productCode => userSubscriptions.has(productCode))
    .reduce((acc, productCode) => acc.concat(inventoryGroups[productCode]), []);
}

export function hasAnySubscriptionUpdates(subscriptions: SubscriptionUpdatesForUser): boolean {
  return Object.keys(subscriptions).some(key =>
    hasSubscriptionUpdatesForBrewery(subscriptions[key].updates),
  );
}

function hasSubscriptionUpdatesForBrewery({
  emptyInventory,
  ...categories
}: BreweryUpdatesForUser): boolean {
  return emptyInventory || Object.keys(categories).some(key => categories[key].length > 0);
}
