import path from 'path';

import { config as dotenv } from 'dotenv-safe';
import { createTransport } from 'nodemailer';
import Email from 'email-templates';

dotenv();

const { MAILER_USER_NAME, MAILER_PASSWORD } = process.env;
const MAILER = createTransport({
  host: 'smtp.gmail.com',
  port: 587,
  secure: false,
  requireTLS: true,
  auth: { user: MAILER_USER_NAME, pass: MAILER_PASSWORD },
});

const ORDERED_STATUSES: ProductStatus[] = ['in', 'limited', 'out'];
const STATUS_SORT_FN = (a: WebsiteProduct, b: WebsiteProduct) =>
  ORDERED_STATUSES.indexOf(a.status) - ORDERED_STATUSES.indexOf(b.status);

interface FormattedBreweryUpdates {
  website: string;
  emptyInventory: boolean;
  subscriptionUpdates: WebsiteProduct[];
  newBeers: WebsiteProduct[];
}

interface TemplateVariables {
  name: string;
  breweryUpdates: { [breweryName: string]: FormattedBreweryUpdates };
}

export async function sendUpdateEmail({
  userInfo,
  subscriptions,
}: {
  userInfo: UserData;
  subscriptions: SubscriptionUpdatesForUser;
}): Promise<void> {
  const email = new Email<TemplateVariables>({
    transport: MAILER,
    htmlToText: {
      // deprecated option but required since the updated selector approach does not work
      uppercaseHeadings: false,
    },
    message: {
      from: 'hm@beer-alerts.com',
      to: userInfo.email,
    },
    juiceResources: {
      webResources: {
        relativeTo: path.resolve(__dirname),
      },
    },
  });

  await email.send({
    template: path.join(__dirname, userInfo.billingInfo ? 'purchase' : 'informational'),
    locals: {
      name: userInfo.name,
      breweryUpdates: Object.entries(subscriptions).reduce(
        (acc, [breweryName, { websiteUrl, updates }]) => {
          const orderedSubscriptionUpdates = [
            ...updates.returningBeers,
            ...updates.updatedBeers,
            ...updates.removedBeers,
          ].sort(STATUS_SORT_FN);
          const orderedNewBeers = updates.newBeerFamilies.sort(STATUS_SORT_FN);

          acc[breweryName] = {
            website: websiteUrl,
            emptyInventory: updates.emptyInventory,
            subscriptionUpdates: orderedSubscriptionUpdates,
            newBeers: orderedNewBeers,
          };
          return acc;
        },
        {} as { [breweryName: string]: FormattedBreweryUpdates },
      ),
    },
  });
}

export function createInventoryPhrase({ name, status, productCode }: WebsiteProduct): string {
  switch (status) {
    case 'in':
      return `${name} (${productCode}) is currently available`;
    case 'limited':
      return `Supplies of ${name} (${productCode}) are running low`;
    case 'out':
      return `${name} recently sold out`;
    default:
      return `${name} is not currently available`;
  }
}
