import { Brewery } from '../db/models';

export async function updateBreweryRecentScanWindow(
  brewery: Brewery,
  latestScan: RecentScanData,
  window = getOldestDate(),
): Promise<void> {
  await Brewery.transaction(trx => {
    return Promise.all([
      brewery
        .$relatedQuery('recentScans')
        .where('scannedAt', '<', window)
        .delete()
        .transacting(trx),
      brewery.$relatedQuery('recentScans').insert(latestScan).transacting(trx),
    ]);
  });
}

function getOldestDate(): Date {
  const date = new Date();
  date.setDate(date.getDate() - 7);

  return date;
}
