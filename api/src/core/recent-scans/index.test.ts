import { Brewery } from '../db/models';

import { updateBreweryRecentScanWindow } from '.';

describe('Recent Scan Tests', () => {
  let brewery: Brewery;

  beforeEach(async () => {
    brewery = await Brewery.query().insert({ name: 'Test Brewery' });
  });

  it('adds a recent scan to a particular brewery', async () => {
    await updateBreweryRecentScanWindow(brewery, {
      scanData: { products: [{ name: 'Foo', productCode: 'foo', productId: '123', status: 'in' }] },
      diff: { added: [], updated: [], removed: [] },
    });

    const updatedScans = await brewery.$relatedQuery('recentScans').orderBy('scannedAt', 'DESC');
    expect(updatedScans).toMatchObject([
      { scanData: { products: [{ name: 'Foo', productCode: 'foo' }] } },
    ]);
  });

  it('rolls off the oldest scans of a particular brewery if their timestamp exceeds the oldest allowed date', async () => {
    const dateInsideWindow = new Date();
    dateInsideWindow.setDate(dateInsideWindow.getDate() - 4);

    const dateOutsideWindow = new Date();
    dateOutsideWindow.setDate(dateOutsideWindow.getDate() - 7);

    await brewery.$relatedQuery('recentScans').insert([
      {
        scanData: { products: [] },
        diff: { added: [], updated: [], removed: [] },
        scannedAt: dateInsideWindow,
      },
      {
        scanData: { products: [] },
        diff: { added: [], updated: [], removed: [] },
        scannedAt: dateOutsideWindow,
      },
    ]);

    await updateBreweryRecentScanWindow(brewery, {
      scanData: { products: [{ name: 'Foo', productCode: 'foo', productId: '123', status: 'in' }] },
      diff: { added: [], updated: [], removed: [] },
    });

    const updatedScans = await brewery.$relatedQuery('recentScans').orderBy('scannedAt', 'DESC');
    expect(updatedScans).toMatchObject([
      { scanData: { products: [{ name: 'Foo', productCode: 'foo' }] } },
      { scanData: { products: [] }, scannedAt: dateInsideWindow },
    ]);
  });
});
