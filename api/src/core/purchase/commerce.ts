import axios, { AxiosResponse } from 'axios';
import { Cookie } from 'set-cookie-parser';

const BASE_COMMERCE_URL = 'https://togo.holymountainbrewing.com/ajax/api/JsonRPC/Commerce';
const BASE_COMMERCE_V2_URL = 'https://togo.holymountainbrewing.com/ajax/api/JsonRPC/CommerceV2';

export async function makeCommerceRequest<T>({
  cookies = [],
  logMessageGenerator,
  method,
  params,
  v2 = false,
}: {
  cookies?: Cookie[];
  method: Commerce.OrderType;
  logMessageGenerator: (err: string) => string;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  params: any[];
  v2?: boolean;
}): Promise<AxiosResponse<Commerce.Response<T>>> {
  const response = await axios.request<Commerce.Response<T>>({
    method: 'POST',
    url: v2 ? BASE_COMMERCE_V2_URL : BASE_COMMERCE_URL,
    headers: {
      contentType: 'application/json',
      cookie: cookies.map(({ name, value }) => `${name}=${value}`).join(';'),
    },
    data: { id: 0, jsonrpc: '2.0', method, params },
  });
  if (response.data.error) {
    const message = response.data.error.message ?? 'An unexpected commerce error occurred';
    console.log(logMessageGenerator(`ERROR: ${message}`));
    throw Error(message);
  }
  if (response.data.result.success === false && response.data.result.message) {
    const message = response.data.result.message ?? 'An unexpected commerce error occurred';
    console.log(logMessageGenerator(`ERROR: ${message}`));
    throw Error(message);
  }

  return response;
}
