import axios from 'axios';
import { Cookie, CookieMap, parse as parseCookies } from 'set-cookie-parser';
import { v4 as uuid } from 'uuid';

import {
  BASE_WEEBLY_URL,
  HM_SQUARE_APPLICATION_ID,
  HM_SQUARE_LOCATION_ID,
  HM_WEEBLY_OWNER_ID,
  HM_WEEBLY_SITE_ID,
  HM_WEEBLY_STORE_ID,
} from '../constants';

import { makeCommerceRequest } from './commerce';

type LogMessageGenerator = (message: string) => string;

export async function purchaseBeer(
  { id }: UserData,
  creditCard: CreditCard,
  { productId, quantity = '1' }: PurchaseData,
): Promise<void> {
  const logMessageGenerator: LogMessageGenerator = message =>
    `${JSON.stringify({ user_id: id, product_id: productId, quantity })} - ${message}`;

  console.log(logMessageGenerator('Initiating purchase flow...'));

  const {
    initialOrder: {
      data: {
        mini_cart: {
          data: {
            order: { uuid: cartId, site_customer_id },
          },
        },
      },
    },
    cookies: { site_session, com_cart_token },
  } = await addItemToCart(productId, Number.parseInt(quantity), logMessageGenerator);
  await setPickupTimeForEarliestTime(cartId, logMessageGenerator, site_session, com_cart_token);
  await addCustomerInfo(
    site_customer_id,
    creditCard,
    logMessageGenerator,
    site_session,
    com_cart_token,
  );
  const { data: cart } = await getCorrectedCart(logMessageGenerator, site_session, com_cart_token);

  const { sessionId } = await hydrateSquareRequest(logMessageGenerator);
  const { card_nonce, card } = await verifyCard(creditCard, sessionId, logMessageGenerator);

  await makePurchase(
    cart,
    card,
    card_nonce,
    creditCard.cvv,
    logMessageGenerator,
    site_session,
    com_cart_token,
  );
}

async function addItemToCart(
  itemId: string,
  quantity: number,
  logMessageGenerator: LogMessageGenerator,
): Promise<{ initialOrder: { data: Commerce.AddItemToCart }; cookies: CookieMap }> {
  console.log(logMessageGenerator(`Adding ${quantity > 1 ? 'items' : 'item'} to cart...`));
  const response = await makeCommerceRequest<Commerce.AddItemToCart>({
    method: 'Checkout::addItemToCart',
    logMessageGenerator,
    params: [
      itemId,
      1,
      quantity,
      {},
      [],
      0,
      { fulfillment_option: 'pickup', store_location_uuid: HM_WEEBLY_STORE_ID },
      {},
    ],
  });
  console.log(logMessageGenerator('Successfully added items to cart'));

  return {
    initialOrder: response.data.result,
    cookies: parseCookies(response.headers['set-cookie'], { map: true }),
  };
}

async function getCorrectedCart(
  logMessageGenerator: LogMessageGenerator,
  ...cookies: Cookie[]
): Promise<Commerce.Result<Commerce.OrderData>> {
  console.log(logMessageGenerator('Fetching corrected cart...'));
  const response = await makeCommerceRequest<Commerce.OrderData>({
    method: 'Order::fetchCorrectedCart',
    logMessageGenerator,
    params: ['web'],
    cookies,
  });
  console.log(logMessageGenerator('Corrected cart fetched'));

  return response.data.result;
}

async function setPickupTimeForEarliestTime(
  cartId: string,
  logMessageGenerator: LogMessageGenerator,
  ...cookies: Cookie[]
) {
  console.log(logMessageGenerator('Finding earliest pickup time...'));
  const scheduleResponse = await axios.request({
    baseURL: `${BASE_WEEBLY_URL}/carts/${cartId}/schedule`,
  });
  const {
    time_unix: unixTime,
    time_formatted: displayTime,
    time_iso8601: isoDateString,
  } = scheduleResponse.data.data.earliest;

  const isoDate = new Date(isoDateString);
  const formattedDate = `${isoDate.getMonth() + 1}/${isoDate.getDate()}/${isoDate.getFullYear()}`;
  const formattedTime = isoDateString.split('T')[1].split('-')[0];

  console.log(logMessageGenerator(`Earliest pickup time is "${displayTime}", updating order...`));
  const updatePickupTimeResponse = await makeCommerceRequest({
    method: 'Order::updatePickupTime',
    logMessageGenerator,
    params: [unixTime, displayTime, formattedDate, formattedTime, null, 'ASAP', false, null],
    cookies,
  });
  console.log(logMessageGenerator('Earliest pickup time set'));

  return updatePickupTimeResponse.data.result;
}

async function addCustomerInfo(
  site_customer_id: string,
  billingInfo: BillingInfoData,
  logMessageGenerator: LogMessageGenerator,
  ...cookies: Cookie[]
) {
  console.log(logMessageGenerator('Adding customer info...'));
  const response = await makeCommerceRequest({
    method: 'Customer::patch',
    logMessageGenerator,
    params: [
      {
        customer: {
          email_address: billingInfo.email,
          family_name: billingInfo.lastName,
          given_name: billingInfo.firstName,
          phone_number: {
            national_number: billingInfo.phone,
            region_code: 'US',
            country_code: '1',
          },
          pickup_phone: {},
        },
        shipping_address: {
          id: uuid().replace(/-/g, ''),
          site_id: HM_WEEBLY_SITE_ID,
          owner_id: HM_WEEBLY_OWNER_ID,
          site_customer_id,
          site_customer_address_id: 1,
          label: 'Shipping',
          first_name: billingInfo.firstName,
          last_name: billingInfo.lastName,
          phone: {
            national_number: billingInfo.phone,
            region_code: 'US',
            country_code: '1',
            formatted: '',
          },
        },
        billing_address: {
          id: uuid().replace(/-/g, ''),
          site_id: HM_WEEBLY_SITE_ID,
          owner_id: HM_WEEBLY_OWNER_ID,
          site_customer_id,
          site_customer_address_id: 2,
          label: 'Billing',
          country: 'US',
          postal_code: '98119',
        },
      },
    ],
    cookies,
    v2: true,
  });
  console.log(logMessageGenerator('Customer info added'));

  return response.data.result;
}

async function hydrateSquareRequest(
  logMessageGenerator: LogMessageGenerator,
): Promise<Square.HydrateResponse> {
  console.log(logMessageGenerator('Initializing Square session...'));
  const response = await axios.request<Square.HydrateResponse>({
    url: `https://pci-connect.squareup.com/payments/hydrate?applicationId=${HM_SQUARE_APPLICATION_ID}&hostname=togo.holymountainbrewing.com&locationId=${HM_SQUARE_LOCATION_ID}`,
  });
  console.log(logMessageGenerator('Square session initialized'));

  return response.data;
}

async function verifyCard(
  card: CreditCard,
  sessionId: string,
  logMessageGenerator: LogMessageGenerator,
): Promise<Square.ValidationResponse> {
  console.log(logMessageGenerator('Verifying card data...'));
  const response = await axios.request<Square.ValidationResponse>({
    method: 'POST',
    url: `https://pci-connect.squareup.com/v2/card-nonce?_=${Date.now()}.${Math.floor(
      Math.random() * 9999,
    )}`,
    data: {
      client_id: HM_SQUARE_APPLICATION_ID,
      location_id: HM_SQUARE_LOCATION_ID,
      session_id: sessionId,
      website_url: 'https://togo.holymountainbrewing.com/',
      card_data: {
        billing_postal_code: card.zipCode,
        cvv: card.cvv,
        exp_month: Number.parseInt(card.expMonth),
        exp_year: Number.parseInt(card.expYear),
        number: card.cardNumber,
      },
    },
  });
  console.log(logMessageGenerator('Card verified'));

  return response.data;
}

async function makePurchase(
  cart: Commerce.OrderData,
  card: Square.Card,
  cardNonce: string,
  cvv: string,
  logMessageGenerator: LogMessageGenerator,
  ...cookies: Cookie[]
) {
  console.log(logMessageGenerator('Attemping item purchase...'));
  await makeCommerceRequest({
    method: 'Order::checkout',
    params: [
      {
        cart_hash: cart.order.cart_hash,
        order_notes: '',
        site_id: HM_WEEBLY_SITE_ID,
        site_order_id: cart.order.site_order_id,
      },
      {
        cashAppToken: '',
        cvv: cvv,
        full_name: '',
        method: 'cc',
        payment_intent_id: '',
        payment_method_id: '',
        update_stored_payment_method: false,
        billing_zip: card.billing_postal_code,
        card: card.last_4,
        card_brand: card.card_brand,
        ccToken: cardNonce,
        exp_month: card.exp_month,
        exp_year: card.exp_year,
        token: cardNonce,
        country: 'US',
      },
      true,
      'webv2',
      { createBillingAddress: false },
    ],
    cookies,
    logMessageGenerator,
  });
  console.log(logMessageGenerator('Purchase succeeded!'));
}
