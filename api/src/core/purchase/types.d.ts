declare namespace Commerce {
  type OrderType =
    | 'Checkout::addItemToCart'
    | 'Order::fetchCorrectedCart'
    | 'Order::updatePickupTime'
    | 'Customer::patch'
    | 'Order::checkout';

  interface Result<T> {
    data: T;
    message?: string;
    success: boolean;
  }

  interface Error {
    message: string;
  }

  interface Response<T> {
    error?: Error;
    result: Result<T>;
  }

  interface OrderData {
    order: {
      cart_hash: string;
      site_customer_id: string;
      site_order_id: string;
      uuid: string;
    };
  }

  interface AddItemToCart {
    mini_cart: Result<OrderData>;
  }
}

declare namespace Square {
  interface HydrateResponse {
    sessionId: string;
  }

  interface Card {
    card_brand: string;
    last_4: string;
    exp_month: number;
    exp_year: number;
    billing_postal_code: string;
  }

  interface ValidationResponse {
    card_nonce: string;
    card: Card;
  }
}
