import * as admin from 'firebase-admin';
import axios from 'axios';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const serviceAccount = require('../../firebase-config.json');

admin.initializeApp({ credential: admin.credential.cert(serviceAccount) });

async function getAuthToken(uid: string) {
  const customToken = await admin.auth().createCustomToken(uid);
  const response = await axios.post<any>(
    'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyCustomToken?key=AIzaSyBxP1N1grBYljJD3NfZ5wk_YJ9YiGs3ZWQ',
    { token: customToken, returnSecureToken: true },
  );
  console.log(response.data.idToken);
}

if (module === require.main) {
  getAuthToken(process.argv[2])
    .then(() => {
      process.exit(0);
    })
    .catch(err => {
      console.log(err);
      process.exit(1);
    });
}
