import * as admin from 'firebase-admin';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const serviceAccount = require('../../firebase-config.json');

admin.initializeApp({ credential: admin.credential.cert(serviceAccount) });

async function run(uid: string, ...newCabilities: string[]) {
  const { customClaims = {} } = await admin.auth().getUser(uid);
  const { capabilities = [] } = customClaims;

  await admin.auth().setCustomUserClaims(uid, {
    ...customClaims,
    capabilities: Array.from(new Set([...capabilities, ...newCabilities])),
  });
}

if (module === require.main) {
  run(process.argv[2], ...process.argv.slice(3))
    .then(() => {
      process.exit(0);
    })
    .catch(err => {
      console.log(err);
      process.exit(1);
    });
}
