import { Application } from 'express';

import { User } from '../../core/db/models';
import { purchaseBeer } from '../../core/purchase';
import { decryptBillingInfo } from '../../core/utils/billing';

import { ApiError, commonHandler, enforceSameUser, enforceUserCapability } from './utils';

export function addPurchaseRoutes(app: Application): void {
  app.post(
    '/purchase',
    enforceSameUser('body'),
    enforceUserCapability('beer_buddy'),
    commonHandler(async ({ body: { userId, cvv, ...rest } }) => {
      if (!cvv || !rest.productId) {
        throw new ApiError(400, 'Missing required purchase data');
      }

      const [user, billingInfo] = await Promise.all([
        User.query().findOne({ id: userId }),
        User.relatedQuery('billingInfo').for(userId).first(),
      ]);
      if (!billingInfo) {
        throw new ApiError(404, 'Billing info not found for user');
      }

      const decryptedBilingInfo = await decryptBillingInfo(
        billingInfo.toJSON(),
        billingInfo.encryptionKey,
      );
      await purchaseBeer(user, { ...decryptedBilingInfo, cvv }, <PurchaseData>rest);

      return { status: 204 };
    }),
  );
}
