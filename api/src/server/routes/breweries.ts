import { Application } from 'express';

import { Brewery } from '../../core/db/models';

import { commonHandler, enforceUserCapability } from './utils';

export function addBreweryRoutes(app: Application): void {
  app.get(
    '/breweries',
    enforceUserCapability('beer_buddy'),
    commonHandler(async () => {
      const breweries = await Brewery.query().withGraphFetched('beerFamilies.beers');
      return {
        data: breweries.reduce(
          (acc, brewery) => Object.assign(acc, { [brewery.name]: brewery }),
          {},
        ),
      };
    }),
  );
  app.get(
    '/breweries/:websiteId',
    enforceUserCapability('beer_buddy'),
    commonHandler<CurrentBreweryData>(async ({ params: { websiteId } }) => {
      const { name, lastScan, beerFamilies } = await Brewery.query()
        .where({ websiteId })
        .withGraphFetched('beerFamilies.beers')
        .first();

      return { data: { name, beers: beerFamilies, currentInventory: lastScan.products } };
    }),
  );
}
