import { Application } from 'express';

import { run } from '../../core/listener';

import { commonHandler, enforceUserCapability } from './utils';

export function addServiceRoutes(app: Application): void {
  app.get(
    '/ping',
    commonHandler(async () => ({ data: 'ping' })),
  );

  app.put(
    '/listener',
    enforceUserCapability('lambda_access'),
    commonHandler(async () => {
      await run();
      return { status: 204 };
    }),
  );

  app.put(
    '/monitor',
    enforceUserCapability('lambda_access'),
    commonHandler(async () => {
      const { performHealthChecks } = await import('../../core/product-scan/hm');
      await performHealthChecks();
      return { status: 204 };
    }),
  );
}
