import { NextFunction, Request, RequestHandler, Response } from 'express';
import * as admin from 'firebase-admin';
import { ValidationError } from 'objection';

import { User } from '../../core/db/models';

type ApiResponse<T> = { status?: number; data?: T | Array<T> | undefined };
type ApiMiddleware = (req: Request, res: Response, next: NextFunction) => Promise<void>;
type ApiFunction<T> = (req: Request, res: Response, next: NextFunction) => Promise<ApiResponse<T>>;

const HANDLED_ERRORS = {
  400: (error: ValidationError | ApiError) => {
    if (error instanceof ApiError) {
      return error.message;
    }
    return Object.entries(error.data).map(([keyword, errors]: [string, unknown[]]) => ({
      [keyword]: errors.map(({ message }) => message),
    }));
  },
  403: ({ message = 'Forbidden' }: ApiError) => message,
  404: ({ message = 'Not Found' }: ApiError) => message,
};

export class ApiError extends Error {
  statusCode: number;

  constructor(statusCode: number, message?: string) {
    super(message);
    Object.setPrototypeOf(this, ApiError.prototype);

    this.statusCode = statusCode;
  }
}

export const enforceAuthorization = ({ except = [] }: { except: string[] }): RequestHandler => {
  const excludedPaths = new Set(except);
  return async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    if (excludedPaths.has(req.path)) {
      next();
    } else {
      const { authorization } = req.headers;
      if (!authorization) {
        res.status(403).json({ error: 'This API endpoint requires authorization' });
        return;
      }
      const [, bearerToken] = authorization.split(/\s+/);
      try {
        const { uid, capabilities = [] } = await admin.auth().verifyIdToken(bearerToken);
        req.authProviderId = uid;
        req.userCapabilities = capabilities;
        next();
      } catch (err) {
        if (err.errorInfo?.code === 'auth/id-token-expired') {
          res.status(403).json({ error: 'This API endpoint requires a non-expired auth token' });
        } else {
          console.log(err);
          res.status(500).json({ error: 'Internal Server Error' });
        }
      }
    }
  };
};

export const enforceUserCapability = (...capabilityOptions: UserCapability[]): RequestHandler => {
  const capabilityLookup = new Set(capabilityOptions);

  return ({ userCapabilities }: Request, res: Response, next: NextFunction) => {
    const canAccess = userCapabilities.reduce(
      (acc, option) => acc || capabilityLookup.has(option),
      false,
    );
    if (!canAccess) {
      res.status(403).json({ error: 'Missing a required capability' });
    } else {
      next();
    }
  };
};

export const commonHandler =
  <T>(routeFn: ApiFunction<T>) =>
  async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { status = 200, data } = await routeFn(req, res, next);
      res.status(status);

      if (data) {
        res.json({ data });
      } else {
        res.end();
      }
    } catch (err) {
      console.log(err);
      const isIntentionalError = err.statusCode && err.statusCode in HANDLED_ERRORS;
      if (isIntentionalError) {
        res.status(err.statusCode).json({ error: HANDLED_ERRORS[err.statusCode](err) });
      } else {
        res.status(500).json({ error: 'Internal Server Error' });
      }
    }
  };

export const enforceSameUser =
  (location: keyof Request): ApiMiddleware =>
  async (
    { authProviderId, [location]: { userId } }: Request,
    res: Response,
    next: NextFunction,
  ): Promise<void> => {
    const associatedUser = await User.query().where({ authProviderId }).first();
    if (associatedUser?.id !== Number.parseInt(userId)) {
      res.status(403).json({ error: 'You are not allowed to access this endpoint' });
    } else {
      next();
    }
  };
