import { Application } from 'express';

import { User } from '../../core/db/models';
import { decryptBillingInfo, encryptBillingInfo } from '../../core/utils/billing';

import { ApiError, commonHandler, enforceSameUser, enforceUserCapability } from './utils';

export function addUserRoutes(app: Application): void {
  app.get(
    '/auth',
    enforceUserCapability('beer_buddy'),
    commonHandler(async ({ authProviderId }) => {
      const associatedUser = await User.query().where({ authProviderId }).returning('id').first();
      if (!associatedUser) {
        throw new ApiError(404);
      }
      return { data: associatedUser };
    }),
  );

  app.post(
    '/users',
    enforceUserCapability('beer_buddy'),
    commonHandler<UserData>(async ({ body }) => {
      return { data: await User.query().insertAndFetch(body) };
    }),
  );

  app.all('/users/:userId*', enforceSameUser('params'));

  app.get(
    '/users/:userId',
    enforceUserCapability('beer_buddy'),
    commonHandler<UserData>(async ({ params: { userId } }) => {
      const user = await User.query()
        .where({ id: userId })
        .withGraphFetched('subscriptions.topic.beers')
        .first();
      if (!user) {
        throw new ApiError(404);
      }

      return { data: user };
    }),
  );

  app.patch(
    '/users/:userId',
    enforceUserCapability('beer_buddy'),
    commonHandler<UserData>(async ({ params: { userId }, body }) => {
      const user = await User.query().where({ id: userId }).first();
      if (!user) {
        throw new ApiError(404);
      }
      const updatedUser = await user
        .$query()
        .patchAndFetch(body)
        .withGraphFetched('subscriptions.topic.beers')
        .first();

      return { data: updatedUser };
    }),
  );

  app.put(
    '/users/:userId/subscriptions/:subscriptionId',
    enforceUserCapability('beer_buddy'),
    commonHandler<SubscriptionData>(async ({ params: { userId, subscriptionId } }) => {
      const existingSubscription = await User.relatedQuery('subscriptions')
        .for(userId)
        .where({ subscriptionId })
        .withGraphFetched('topic.beers')
        .first();
      if (existingSubscription) {
        return { data: existingSubscription };
      }
      return {
        data: await User.relatedQuery('subscriptions')
          .for(Number.parseInt(userId))
          .insertAndFetch({ subscriptionId: Number.parseInt(subscriptionId) })
          .withGraphFetched('topic.beers')
          .skipUndefined(),
      };
    }),
  );
  app.delete(
    '/users/:userId/subscriptions/:subscriptionId',
    enforceUserCapability('beer_buddy'),
    commonHandler(async ({ params: { userId, subscriptionId } }) => {
      await User.relatedQuery('subscriptions').for(userId).where({ subscriptionId }).delete();

      return { status: 204 };
    }),
  );

  app.put(
    '/users/:userId/billing',
    enforceUserCapability('beer_buddy'),
    commonHandler(async ({ params: { userId }, body }) => {
      const newBillingInfo = await User.transaction(async trx => {
        await User.relatedQuery('billingInfo').for(userId).transacting(trx).delete();
        return User.relatedQuery('billingInfo')
          .for(userId)
          .transacting(trx)
          .insertAndFetch(await encryptBillingInfo(body));
      });

      return { data: newBillingInfo };
    }),
  );

  app.get(
    '/users/:userId/billing',
    enforceUserCapability('beer_buddy'),
    commonHandler(async ({ params: { userId } }) => {
      const billingInfo = await User.relatedQuery('billingInfo').for(userId).first();
      if (!billingInfo) {
        return { status: 204 };
      }

      return { data: await decryptBillingInfo(billingInfo.toJSON(), billingInfo.encryptionKey) };
    }),
  );

  app.delete(
    '/users/:userId/billing',
    enforceUserCapability('beer_buddy'),
    commonHandler(async ({ params: { userId } }) => {
      await User.relatedQuery('billingInfo').for(userId).delete();

      return { status: 204 };
    }),
  );
}
