import { Application } from 'express';

import { addUserRoutes } from './users';
import { addBreweryRoutes } from './breweries';
import { addPurchaseRoutes } from './purchase';
import { enforceAuthorization } from './utils';
import { addServiceRoutes } from './service';

export const loadRoutes = (app: Application): void => {
  app.use(enforceAuthorization({ except: ['/ping'] }));
  addUserRoutes(app);
  addBreweryRoutes(app);
  addServiceRoutes(app);
  addPurchaseRoutes(app);
};
