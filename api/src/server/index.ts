import * as admin from 'firebase-admin';
import dotenv from 'dotenv-safe';
import express from 'express';
import cors from 'cors';
import morgan from 'morgan';
import { v4 as uuid } from 'uuid';

import { loadRoutes } from './routes';

dotenv.config();

const PORT = 3000;

admin.initializeApp({
  credential: admin.credential.cert({
    clientEmail: process.env.FIREBASE_CLIENT_EMAIL,
    privateKey: process.env.FIREBASE_PRIVATE_KEY,
    projectId: process.env.FIREBASE_PROJECT_ID,
  }),
});

morgan.token('requestId', () => `request id: ${uuid()}`);

async function runApi() {
  const application = express();

  application.use(cors());
  application.use(express.json());
  application.use(
    morgan((tokens, req, res) => {
      return [
        tokens.requestId(req, res),
        tokens.method(req, res),
        tokens.url(req, res),
        tokens.status(req, res),
        tokens.res(req, res, 'content-length'),
        '-',
        tokens['response-time'](req, res),
        'ms',
      ].join(' ');
    }),
  );

  loadRoutes(application);

  application.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`);
  });
}

if (module === require.main) {
  runApi();
}
