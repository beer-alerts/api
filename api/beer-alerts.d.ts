type ProductLoadFunction = () => Promise<ScannedProduct[]>;

type ProductStatus = 'in' | 'limited' | 'out';

interface Product {
  productCode: string;
}

interface WebsiteProduct extends Product {
  name: string;
  productId: string;
  probableFamilyCode?: string;
  status: ProductStatus;
}

interface ScannedProduct extends WebsiteProduct {
  description: string;
}

interface WebsiteScanResult {
  products: ScannedProduct[];
}

interface RecentScanData {
  scanData: { products: WebsiteProduct[] };
  diff: {
    added: WebsiteProduct[];
    updated: WebsiteProduct[];
    removed: WebsiteProduct[];
  };
}

interface InventoryUpdate {
  breweryId: number;
  latestProducts: WebsiteProduct[];
  newBeerFamilies: WebsiteProduct[];
  returningBeers: WebsiteProduct[];
  updatedBeers: WebsiteProduct[];
  removedBeers: WebsiteProduct[];
}

interface ProductGroups {
  [familyId: string]: WebsiteProduct[];
}

interface GroupedInventoryUpdate {
  latestInventory: WebsiteProduct[];
  newBeerFamilies: WebsiteProduct[];
  returningBeers: ProductGroups;
  updatedBeers: ProductGroups;
  removedBeers: ProductGroups;
}

interface BreweryUpdatesForUser {
  emptyInventory: boolean;
  newBeerFamilies: WebsiteProduct[];
  returningBeers: WebsiteProduct[];
  updatedBeers: WebsiteProduct[];
  removedBeers: WebsiteProduct[];
}

interface SubscriptionUpdatesForUser {
  [breweryName: string]: {
    websiteUrl: string;
    updates: BreweryUpdatesForUser;
  };
}

type SubscriptionCodeLookup = {
  [breweryId: string]: Set<string>;
};

interface InventoryByBrewery {
  [breweryId: string]: {
    name: string;
    website: string;
    latestProducts: ScannedProduct[];
    inventoryUpdates: GroupedInventoryUpdate;
  };
}

interface UserInfo {
  id: number;
  name: string;
  email: string;
}

interface UserData extends UserInfo {
  subscriptions?: SubscriptionData[];
  billingInfo?: BillingInfoData;
}

interface BillingInfoData {
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  cardNumber: string;
  expMonth: string;
  expYear: string;
  zipCode: string;
}

interface StoredBillingInfo extends BillingInfoData {
  encryptionKey: string;
}

interface CreditCard extends BillingInfoData {
  cvv: string;
}

interface PurchaseData {
  productId: string;
  quantity?: string;
}

interface SubscriptionData {
  topic: BeerFamilyData;
}

interface BeerFamilyData extends Product {
  name: string;
  abbreviation: string;
  beers: BeerData[];
}

interface BeerData extends Product {
  name: string;
}

interface CurrentBreweryData {
  name: string;
  currentInventory: WebsiteProduct[];
  beers: BeerFamilyData[];
}

type UserCapability = 'beer_buddy' | 'lambda_access' | 'admin';

declare namespace Response {}

declare namespace Express {
  export interface Request {
    authProviderId: string;
    userCapabilities: UserCapability[];
  }
}
