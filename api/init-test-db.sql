CREATE USER beer_alerts_test WITH ENCRYPTED PASSWORD 'beer_alerts';
CREATE DATABASE beer_alerts_test;
\connect beer_alerts_test
CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;
GRANT ALL PRIVILEGES ON DATABASE beer_alerts_test TO beer_alerts_test;