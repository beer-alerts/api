import cleaner from 'knex-cleaner';

import { knex } from '../src/core/db/models';

beforeEach(async () => {
  await cleaner.clean(knex);
});

afterAll(async () => {
  await knex.destroy();
});
