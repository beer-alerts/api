#!/bin/bash

(
  set -e
  profile="$1"
  account_number=$(aws sts get-caller-identity --profile $profile --query "Account" | xargs)

  SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
  cd $SCRIPT_DIR/..

  aws ecr get-login-password \
    --region us-west-2 \
    --profile $profile | \
    docker login \
      --username AWS \
      --password-stdin \
      $account_number.dkr.ecr.us-west-2.amazonaws.com
  docker build \
    --cache-from $account_number.dkr.ecr.us-west-2.amazonaws.com/ba-api-repository:latest \
    -t $account_number.dkr.ecr.us-west-2.amazonaws.com/ba-api-repository:$(git rev-parse HEAD) \
    -t $account_number.dkr.ecr.us-west-2.amazonaws.com/ba-api-repository:latest \
    -f Dockerfile.api \
    --platform linux/amd64 \
    --target prod .

  docker push $account_number.dkr.ecr.us-west-2.amazonaws.com/ba-api-repository:$(git rev-parse HEAD)
  docker push $account_number.dkr.ecr.us-west-2.amazonaws.com/ba-api-repository:latest
)