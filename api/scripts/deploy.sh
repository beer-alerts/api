#!/bin/bash

(
  set -e
  profile="$1"
  
  SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
  
  $SCRIPT_DIR/push.sh $profile

  aws ecs update-service \
    --profile $profile \
    --cluster ba-cluster \
    --service ba-api-service \
    --force-new-deployment
)