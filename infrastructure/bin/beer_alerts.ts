#!/usr/bin/env node
import * as cdk from '@aws-cdk/core';
import 'source-map-support/register';
import {BedrockStack} from '../src/bedrock';
import {DatabaseStack} from '../src/db';
import {ClusterStack} from '../src/cluster';
import {NetworkingStack} from '../src/networking';
import {ApplicationStack} from '../src/application';

const app = new cdk.App();

const {
  apiRepository,
  websiteRepository,
  domain,
  certificate,
  dataEncryptionGeneratorKey,
  accountNumber,
} = new BedrockStack(app);
const {vpc} = new NetworkingStack(app);
const {cluster} = new ClusterStack(app, vpc);
const {database} = new DatabaseStack(app, vpc, cluster);

new ApplicationStack(
  app,
  vpc,
  cluster,
  certificate,
  database,
  domain,
  apiRepository,
  websiteRepository,
  dataEncryptionGeneratorKey,
  accountNumber,
);
