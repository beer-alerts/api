import * as cdk from '@aws-cdk/core';
import {DatabaseInstance, DatabaseInstanceEngine, PostgresEngineVersion} from '@aws-cdk/aws-rds';
import {
  InstanceClass,
  InstanceSize,
  InstanceType,
  Port,
  SecurityGroup,
  SubnetType,
  Vpc,
} from '@aws-cdk/aws-ec2';
import {SecretValue} from '@aws-cdk/core';
import {Cluster} from '@aws-cdk/aws-ecs';

export class DatabaseStack extends cdk.Stack {
  database: DatabaseInstance;

  constructor(scope: cdk.Construct, vpc: Vpc, cluster: Cluster, props?: cdk.StackProps) {
    super(scope, 'beer-alerts-database-stack', props);

    const dbSecurityGroup = new SecurityGroup(this, 'database-security-group', {
      vpc,
      securityGroupName: 'ba-database-sg',
    });

    this.database = new DatabaseInstance(this, 'ba-database', {
      credentials: {
        username: 'beer_alerts_admin',
        password: SecretValue.ssmSecure('DB_ADMIN_PASSWORD', '1'),
      },
      databaseName: 'beer_alerts',
      engine: DatabaseInstanceEngine.postgres({
        version: PostgresEngineVersion.VER_12,
      }),
      allocatedStorage: 20,
      instanceIdentifier: 'ba-database',
      instanceType: InstanceType.of(InstanceClass.T2, InstanceSize.MICRO),
      securityGroups: [dbSecurityGroup],
      vpc,
      vpcSubnets: {subnetType: SubnetType.PRIVATE_ISOLATED, onePerAz: true},
    });

    cluster.connections.securityGroups.forEach(clusterSg => {
      dbSecurityGroup.connections.allowTo(clusterSg, Port.tcp(5432));
      dbSecurityGroup.connections.allowFrom(clusterSg, Port.tcp(5432));
    });
  }
}
