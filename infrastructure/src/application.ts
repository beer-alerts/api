import * as cdk from '@aws-cdk/core';
import {Repository} from '@aws-cdk/aws-ecr';
import {SubnetType, Vpc} from '@aws-cdk/aws-ec2';
import {
  Cluster,
  ContainerImage,
  LogDriver,
  Ec2Service,
  TaskDefinition,
  Compatibility,
  Secret,
} from '@aws-cdk/aws-ecs';
import {DatabaseInstance} from '@aws-cdk/aws-rds';
import {
  ApplicationLoadBalancer,
  ApplicationProtocol,
  ApplicationTargetGroup,
  ListenerAction,
  ListenerCondition,
} from '@aws-cdk/aws-elasticloadbalancingv2';
import {Key} from '@aws-cdk/aws-kms';
import {LogGroup} from '@aws-cdk/aws-logs';
import {StringParameter} from '@aws-cdk/aws-ssm';
import {Certificate} from '@aws-cdk/aws-certificatemanager';
import {ARecord, HostedZone, RecordTarget} from '@aws-cdk/aws-route53';
import {Code, Function, Runtime} from '@aws-cdk/aws-lambda';
import {Rule, Schedule} from '@aws-cdk/aws-events';
import {LambdaFunction} from '@aws-cdk/aws-events-targets';
import {Duration, RemovalPolicy} from '@aws-cdk/core';
import {LoadBalancerTarget} from '@aws-cdk/aws-route53-targets';
import {DnsValidatedDomainIdentity} from 'aws-cdk-ses-domain-identity';

export class ApplicationStack extends cdk.Stack {
  constructor(
    scope: cdk.Construct,
    vpc: Vpc,
    cluster: Cluster,
    certificate: Certificate,
    database: DatabaseInstance,
    domain: HostedZone,
    apiRepository: Repository,
    websiteRepository: Repository,
    dataEncryptionGeneratorKey: Key,
    accountNumber: string,
    props?: cdk.StackProps,
  ) {
    super(scope, 'beer-alerts-application-stack', props);

    const mailerPasswordParameter = StringParameter.fromSecureStringParameterAttributes(
      this,
      'MAILER_PASSWORD',
      {parameterName: 'MAILER_PASSWORD', version: 1},
    );
    const workerPasswordParameter = StringParameter.fromStringParameterAttributes(
      this,
      'WORKER_PASSWORD',
      {
        parameterName: 'WORKER_PASSWORD',
      },
    );

    const apiTaskDefinition = new TaskDefinition(this, 'ba-api-task-def', {
      compatibility: Compatibility.EC2,
      family: 'ba-api-task',
    });
    apiTaskDefinition.addContainer('ba-api-container-def', {
      image: ContainerImage.fromEcrRepository(apiRepository, 'latest'),
      containerName: 'api',
      portMappings: [{hostPort: 3000, containerPort: 3000}],
      environment: {
        NODE_ENV: 'production',
        DB_HOST: database.dbInstanceEndpointAddress,
        MAILER_USER_NAME: StringParameter.valueForStringParameter(this, 'MAILER_USER_NAME'),
        AWS_ACCOUNT_NUMBER: accountNumber,
        FIREBASE_CLIENT_EMAIL: StringParameter.valueForStringParameter(
          this,
          'FIREBASE_CLIENT_EMAIL',
        ),
        FIREBASE_PROJECT_ID: StringParameter.valueForStringParameter(this, 'FIREBASE_PROJECT_ID'),
      },
      secrets: {
        MAILER_PASSWORD: Secret.fromSsmParameter(mailerPasswordParameter),
        DB_ADMIN_PASSWORD: Secret.fromSsmParameter(
          StringParameter.fromSecureStringParameterAttributes(
            this,
            'ba-api-db-admin-password-secret',
            {parameterName: 'DB_ADMIN_PASSWORD', version: 1},
          ),
        ),
        FIREBASE_PRIVATE_KEY: Secret.fromSsmParameter(
          StringParameter.fromSecureStringParameterAttributes(
            this,
            'ba-api-firebase-private-key-secret',
            {parameterName: 'FIREBASE_PRIVATE_KEY', version: 1},
          ),
        ),
      },
      logging: LogDriver.awsLogs({
        streamPrefix: 'ba-api-service',
        logGroup: new LogGroup(this, 'ba-api-logs', {
          logGroupName: 'ba-api-logs',
          removalPolicy: RemovalPolicy.DESTROY,
        }),
      }),
      memoryLimitMiB: 300,
    });

    const apiService = new Ec2Service(this, 'ba-api-service', {
      serviceName: 'ba-api-service',
      cluster,
      taskDefinition: apiTaskDefinition,
      minHealthyPercent: 0,
    });
    dataEncryptionGeneratorKey.grantEncryptDecrypt(apiService.taskDefinition.taskRole);

    const apiTargetGroup = new ApplicationTargetGroup(this, 'ba-api-target-group', {
      vpc,
      targetGroupName: 'ba-api-target-group',
      healthCheck: {path: '/ping'},
      deregistrationDelay: Duration.seconds(10),
      port: 3000,
      protocol: ApplicationProtocol.HTTP,
      targets: [apiService],
    });
    const websiteTaskDefinition = new TaskDefinition(this, 'ba-website-task-def', {
      compatibility: Compatibility.EC2,
      family: 'ba-website-task',
    });
    websiteTaskDefinition.addContainer('ba-website-container-def', {
      image: ContainerImage.fromEcrRepository(websiteRepository, 'latest'),
      containerName: 'website',
      portMappings: [{hostPort: 80, containerPort: 80}],
      logging: LogDriver.awsLogs({
        streamPrefix: 'ba-website-service',
        logGroup: new LogGroup(this, 'ba-website-logs', {
          logGroupName: 'ba-website-logs',
          removalPolicy: RemovalPolicy.DESTROY,
        }),
      }),
      memoryLimitMiB: 300,
    });

    const websiteTargetGroup = new ApplicationTargetGroup(this, 'ba-website-target-group', {
      vpc,
      targetGroupName: 'ba-website-target-group',
      deregistrationDelay: Duration.seconds(10),
      protocol: ApplicationProtocol.HTTP,
      targets: [
        new Ec2Service(this, 'ba-website-service', {
          serviceName: 'ba-website-service',
          cluster,
          taskDefinition: websiteTaskDefinition,
          minHealthyPercent: 0,
        }),
      ],
    });

    const loadBalancer = new ApplicationLoadBalancer(this, 'ba-application-lb', {
      vpc,
      loadBalancerName: 'ba-application-lb',
      vpcSubnets: {subnetType: SubnetType.PUBLIC},
      internetFacing: true,
    });

    loadBalancer.addListener('ba-http-listener', {
      open: true,
      protocol: ApplicationProtocol.HTTP,
      defaultAction: ListenerAction.redirect({
        protocol: ApplicationProtocol.HTTPS,
        port: '443',
        permanent: true,
      }),
    });
    const listener = loadBalancer.addListener('ba-https-listener', {
      certificates: [certificate],
      open: true,
      protocol: ApplicationProtocol.HTTPS,
      defaultTargetGroups: [websiteTargetGroup],
    });
    listener.addAction('public-www-action', {
      conditions: [ListenerCondition.hostHeaders(['www.beer-alerts.com'])],
      action: ListenerAction.forward([websiteTargetGroup]),
      priority: 2,
    });

    listener.addAction('public-api-action', {
      conditions: [ListenerCondition.hostHeaders(['api.beer-alerts.com'])],
      action: ListenerAction.forward([apiTargetGroup]),
      priority: 3,
    });
    listener.addAction('redirect-bare-to-www-action', {
      conditions: [ListenerCondition.hostHeaders(['beer-alerts.com'])],
      action: ListenerAction.redirect({host: 'www.#{host}'}),
      priority: 4,
    });

    ['www', 'api'].forEach(
      recordName =>
        new ARecord(this, `ba-${recordName}-aname-record`, {
          zone: domain,
          recordName,
          target: RecordTarget.fromAlias(new LoadBalancerTarget(loadBalancer)),
        }),
    );

    const workerUserName = StringParameter.valueForStringParameter(this, 'WORKER_USER_NAME');

    const apiUrl = `https://api.${domain.zoneName}`;
    new Rule(this, 'ba-listener-schedule-rule', {
      ruleName: 'ba-listener-schedule-rule',
      description: 'triggers an on-the-minute check for new beers',
      schedule: Schedule.expression('cron(0/1 * * * ? *)'),
      targets: [
        new LambdaFunction(
          new Function(this, 'ba-listener-lambda', {
            functionName: 'ba-listener-lambda',
            runtime: Runtime.NODEJS_14_X,
            code: Code.fromDockerBuild(`${__dirname}/../../listener-lambda`),
            handler: 'index.handler',
            environment: {
              WORKER_USER_NAME: workerUserName,
              WORKER_PASSWORD: workerPasswordParameter.stringValue,
              BASE_URL: apiUrl,
            },
            retryAttempts: 0,
          }),
        ),
      ],
    });

    new Rule(this, 'ba-monitor-schedule-rule', {
      ruleName: 'ba-monitor-schedule-rule',
      description: 'runs twice a day to sanity check the business logic',
      schedule: Schedule.expression('cron(0 6/12 * * ? *)'),
      targets: [
        new LambdaFunction(
          new Function(this, 'ba-monitor-lambda', {
            functionName: 'ba-monitor-lambda',
            runtime: Runtime.NODEJS_14_X,
            code: Code.fromDockerBuild(`${__dirname}/../../monitor-lambda`),
            handler: 'index.handler',
            environment: {
              WORKER_USER_NAME: workerUserName,
              WORKER_PASSWORD: workerPasswordParameter.stringValue,
              BASE_URL: apiUrl,
            },
            retryAttempts: 0,
          }),
        ),
      ],
    });

    new DnsValidatedDomainIdentity(this, 'ba-validated-email-domain', {
      domainName: 'beer-alerts.com',
      hostedZone: domain,
    });
  }
}
