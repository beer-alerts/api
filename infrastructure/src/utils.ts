import {RecordType} from '@aws-cdk/aws-route53';
import {readFileSync} from 'fs';

const ignoredDNSRecordTypes = new Set(['SOA', 'NS']);

export interface DomainEntry {
  domain: string;
  ttl: number;
  type: RecordType;
  record: string;
}

export function getRelevantGoogleDomainEntries(): DomainEntry[] {
  return readFileSync(`${__dirname}/../google_domain_info.txt`)
    .toString()
    .split('\n')
    .filter(line => !!line && !line.startsWith(';'))
    .map(line => line.split(/\s+/))
    .filter(([, , , type]) => !ignoredDNSRecordTypes.has(type))
    .map(([domain, ttl, , type, ...rest]) => ({
      domain,
      ttl: Number.parseInt(ttl),
      type: type as RecordType,
      record: rest.join(' '),
    }));
}
