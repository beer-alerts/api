import * as cdk from '@aws-cdk/core';
import {InstanceClass, InstanceSize, InstanceType, Port, SubnetType, Vpc} from '@aws-cdk/aws-ec2';
import {Cluster} from '@aws-cdk/aws-ecs';
import {CfnOutput, Stack} from '@aws-cdk/core';

export class ClusterStack extends Stack {
  cluster: Cluster;

  constructor(scope: cdk.Construct, vpc: Vpc, props?: cdk.StackProps) {
    super(scope, 'beer-alerts-cluster-stack', props);
    this.cluster = new Cluster(this, 'ba-cluster', {
      clusterName: 'ba-cluster',
      vpc,
      capacity: {
        instanceType: InstanceType.of(InstanceClass.T2, InstanceSize.MICRO),
        vpcSubnets: {subnetType: SubnetType.PUBLIC},
        maxCapacity: 1,
        keyName: 'beer_alerts',
      },
    });

    this.cluster.connections.allowFromAnyIpv4(Port.tcp(22));

    if (this.cluster.autoscalingGroup) {
      new CfnOutput(this, 'autoscaling-group-name', {
        exportName: 'beeralertsasgname',
        value: this.cluster.autoscalingGroup.autoScalingGroupName,
      });
    }
  }
}
