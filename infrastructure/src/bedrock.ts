import * as cdk from '@aws-cdk/core';
import {Repository, TagStatus} from '@aws-cdk/aws-ecr';
import {Certificate, DnsValidatedCertificate} from '@aws-cdk/aws-certificatemanager';
import {HostedZone, RecordSet, RecordTarget, RecordType} from '@aws-cdk/aws-route53';
import {CfnOutput, Duration} from '@aws-cdk/core';
import {Key} from '@aws-cdk/aws-kms';
import {getRelevantGoogleDomainEntries} from './utils';

export class BedrockStack extends cdk.Stack {
  apiRepository: Repository;
  websiteRepository: Repository;
  certificate: Certificate;
  domain: HostedZone;
  dataEncryptionGeneratorKey: Key;
  accountNumber: string;

  constructor(scope: cdk.Construct, props?: cdk.StackProps) {
    super(scope, 'beer-alerts-bedrock-stack', props);

    this.apiRepository = new Repository(this, 'ba-api-repository', {
      repositoryName: 'ba-api-repository',
      lifecycleRules: [
        {
          description: 'Only retain one latest tag',
          tagPrefixList: ['latest'],
          maxImageCount: 1,
        },
        {
          description: 'Remove all non-latest images',
          tagStatus: TagStatus.ANY,
          maxImageCount: 1,
        },
      ],
    });

    this.websiteRepository = new Repository(this, 'ba-website-repository', {
      repositoryName: 'ba-website-repository',
      lifecycleRules: [
        {
          description: 'Only retain one latest tag',
          tagPrefixList: ['latest'],
          maxImageCount: 1,
        },
        {
          description: 'Remove all non-latest images',
          tagStatus: TagStatus.ANY,
          maxImageCount: 1,
        },
      ],
    });

    this.domain = new HostedZone(this, 'ba-domain', {
      zoneName: 'beer-alerts.com',
      comment: 'CDK-based Hosted Zone for Beer Alerts',
    });
    this.certificate = new DnsValidatedCertificate(this, 'ba-certificate', {
      domainName: 'beer-alerts.com',
      hostedZone: this.domain,
      subjectAlternativeNames: ['www.beer-alerts.com', 'api.beer-alerts.com'],
    });

    const googleDomainEntries = getRelevantGoogleDomainEntries();
    const recordsGroupedByType = googleDomainEntries.reduce(
      (acc, {domain, type, record, ttl}) => {
        const masterRecord = acc[type] || {
          recordName: domain.replace(/\.?beer-alerts.com\./, ''),
          type,
          ttl,
          entries: [],
        };
        masterRecord.entries.push(record);
        acc[type] = masterRecord;

        return acc;
      },
      {} as {
        [key: string]: {
          recordName: string;
          type: RecordType;
          ttl: number;
          entries: string[];
        };
      },
    );
    Object.values(recordsGroupedByType).forEach(({recordName, type, entries, ttl}, i) => {
      new RecordSet(this, `ba-dns-record-${i}`, {
        zone: this.domain,
        recordName: recordName.length > 0 ? recordName : undefined,
        recordType: type,
        ttl: Duration.seconds(ttl),
        target: RecordTarget.fromValues(...entries),
      });
    });

    this.dataEncryptionGeneratorKey = new Key(this, 'ba-data-encryption-generator-key', {
      alias: 'data-encryption-generator-key',
      description: 'Generate data keys to encrypt and decrypt user credit card info',
    });

    this.accountNumber = new CfnOutput(this, 'account-number', {
      exportName: 'accoutnumber',
      value: this.account,
    }).value;
  }
}
