import * as cdk from '@aws-cdk/core';
import {Vpc, SubnetType} from '@aws-cdk/aws-ec2';

export class NetworkingStack extends cdk.Stack {
  vpc: Vpc;
  constructor(scope: cdk.Construct, props?: cdk.StackProps) {
    super(scope, 'beer-alerts-networking-stack', props);

    this.vpc = new Vpc(this, 'ba-vpc', {
      subnetConfiguration: [
        {name: 'public', subnetType: SubnetType.PUBLIC},
        {name: 'private', subnetType: SubnetType.PRIVATE_ISOLATED},
      ],
    });
  }
}
