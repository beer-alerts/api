#!/bin/bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

db_addr="$1"
db_admin_password="$DB_ADMIN_PASSWORD"

psql "postgres://beer_alerts_admin:$db_admin_password@$db_addr/postgres" -f $SCRIPT_DIR/init-db.sql