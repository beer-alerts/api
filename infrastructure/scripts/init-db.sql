CREATE DATABASE beer_alerts;
\connect beer_alerts
CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;
GRANT ALL PRIVILEGES ON DATABASE beer_alerts TO beer_alerts_admin;