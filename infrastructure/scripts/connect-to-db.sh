#!/bin/bash

set -e

profile="${1:-ba}"
db_addr="${2:-ba-database.cow6afvtupdl.us-west-2.rds.amazonaws.com}"
host_port=${3:-5433}

stty -echoctl

echo Discovering active beer alerts EC2 instance...

asgname="$(aws cloudformation --profile $profile list-exports --query "Exports[?(Name == 'beeralertsasgname')] | [0].Value" | xargs)"
ec2_addr=$(aws ec2 describe-instances --profile $profile --query "Reservations[].Instances[?(State.Name == 'running' && Tags[?(Key=='aws:autoscaling:groupName' && Value=='$asgname')])][] | [0].PublicDnsName" | xargs)

echo Connecting to $db_addr via $ec2_addr...

echo $ec2_addr
echo "About to forward to database, connection will remain open as long as this script is running, verify connection with \"lsof -t -i :$host_port\""
ssh -N -i beer_alerts.pem -L $host_port:$db_addr:5432 ec2-user@$ec2_addr

echo "Bye!"
stty echoctl

