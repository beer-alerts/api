const axios = require('axios').default;
const firebase = require('firebase');

firebase.initializeApp({
  apiKey: "AIzaSyBxP1N1grBYljJD3NfZ5wk_YJ9YiGs3ZWQ",
  authDomain: "beer-alerts.firebaseapp.com",
  databaseURL: "https://beer-alerts.firebaseio.com",
  projectId: "beer-alerts",
  storageBucket: "beer-alerts.appspot.com",
  messagingSenderId: "884726079486",
  appId: "1:884726079486:web:60c81907131a3f9bf2eaaf"
});

exports.handler = async () => {
  const auth = firebase.auth();

  const { user } = await auth.signInWithEmailAndPassword(process.env.WORKER_USER_NAME, process.env.WORKER_PASSWORD);
  console.log(user);
  const token = await user.getIdToken();
  console.log(token);
  
  const response = await axios.put(`${process.env.BASE_URL}/listener`, {}, { headers: { authorization: `Bearer ${token}` } });
  console.log(response.status, response.data);
}

if (module === require.main) {
  exports.handler();
}