const { SES } = require ('aws-sdk');
const axios = require('axios').default;
const firebase = require('firebase');

firebase.initializeApp({
  apiKey: "AIzaSyBxP1N1grBYljJD3NfZ5wk_YJ9YiGs3ZWQ",
  authDomain: "beer-alerts.firebaseapp.com",
  databaseURL: "https://beer-alerts.firebaseio.com",
  projectId: "beer-alerts",
  storageBucket: "beer-alerts.appspot.com",
  messagingSenderId: "884726079486",
  appId: "1:884726079486:web:60c81907131a3f9bf2eaaf"
});

exports.handler = async () => {
  const auth = firebase.auth();

  const { user } = await auth.signInWithEmailAndPassword(process.env.WORKER_USER_NAME, process.env.WORKER_PASSWORD);
  const token = await user.getIdToken();

  try {
    const response = await axios.put(`${process.env.BASE_URL}/monitor`, {}, { headers: { authorization: `Bearer ${token}` } });
    console.log(response.status, response.data);
  } catch (err) {
    let message;
  
    if (err.response) {
      console.log(err.response);
      message = `Unexpected error when monitoring the site with status: ${err.response.status}, investigate the logs`;
    } else if (err.request) {
      console.log(err.request);
      message = 'Unable to make monitoring request to API, is the service up?';
    } else {
      console.log(err.message);
      message = 'Something went wrong running the monitor lambda';
    }

    const response = await new SES().sendEmail({
      Source: process.env.WORKER_USER_NAME,
      Destination: { ToAddresses: ['monitor-alerts@beer-alerts.com'] }, 
      Message: {
        Subject: { Data: 'Beer Alerts Site Scraping Issue!'},
        Body: { Text: { Charset: "UTF-8", Data: message } },
      },
    }).promise();
    console.log(response);
  }
}